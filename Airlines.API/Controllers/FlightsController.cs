﻿using Airlines.Business.DTOs;
using Airlines.Business.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Airlines.API.Controllers;

[ApiController]
[Route("flights")]
public class FlightsController : ControllerBase
{
    private readonly IFlightService _flightService;
    public FlightsController(IFlightService flightService)
    {
        this._flightService = flightService;
    }
    [HttpGet("all")]
    public async Task<IActionResult> GetAll()
    {
        var allFlights = await this._flightService.GetAll().ToListAsync();
        return Ok(allFlights);
    }
    [HttpGet("{id}")]
    public async Task<IActionResult> GetById(int id)
    {
        var flight = await this._flightService.GetFlightById(id);

        if (flight == null)
        {
            return NotFound($"Flight with specified id {id} does not exist");
        }

        return Ok(flight);
    }

    [HttpPost("create")]
    public async Task<IActionResult> Create([FromBody] FlightDTO flightDTO)
    {
        await this._flightService.AddFlight(flightDTO);
        return Created();
    }

    [HttpPut("edit")]
    public async Task<IActionResult> Edit([FromBody] FlightDTO flightDTO)
    {
        await this._flightService.UpdateFlight(flightDTO, flightDTO.Id);
        return Ok();
    }

    [HttpDelete("delete/{id}")]
    public async Task<IActionResult> Delete(int id)
    {
        await this._flightService.DeleteFlight(id);
        return Ok();
    }
}
