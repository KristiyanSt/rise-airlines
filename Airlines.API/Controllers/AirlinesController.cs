﻿using Airlines.Business.DTOs;
using Airlines.Business.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Airlines.API.Controllers;

[ApiController]
[Route("airlines")]
public class AirlinesController : ControllerBase
{
    private readonly IAirlineService _airlineService;
    public AirlinesController(IAirlineService airlineService)
    {
        this._airlineService = airlineService;
    }
    [HttpGet("all")]
    public async Task<IActionResult> GetAll()
    {
        var allAirlines = await this._airlineService.GetAll().ToListAsync();
        return Ok(allAirlines);
    }
    [HttpGet("{name}")]
    public async Task<IActionResult> GetByName(string name)
    {
        if (String.IsNullOrEmpty(name))
        {
            return BadRequest("Invalid airline name format");
        }

        var airline = await this._airlineService.GetAirlineByName(name);

        if (airline == null)
        {
            return NotFound($"Airline with specified name {name} does not exist");
        }

        return Ok(airline);
    }

    [HttpPost("create")]
    public async Task<IActionResult> Create([FromBody] AirlineDTO airlineDTO)
    {
        await this._airlineService.AddAirline(airlineDTO);
        return Created();
    }

    [HttpPut("edit")]
    public async Task<IActionResult> Edit([FromBody] AirlineDTO airlineDTO)
    {
        await this._airlineService.UpdateAirline(airlineDTO, airlineDTO.Name);
        return Ok();
    }
    [HttpDelete("delete/{name}")]
    public async Task<IActionResult> Delete(string name)
    {
        await this._airlineService.DeleteAirline(name);
        return Ok();
    }
}
