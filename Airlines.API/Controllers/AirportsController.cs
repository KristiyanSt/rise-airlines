﻿using Airlines.Business.DTOs;
using Airlines.Business.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Airlines.API.Controllers;

[ApiController]
[Route("airports")]
public class AirportsController : ControllerBase
{
    private readonly IAirportService _airportService;
    public AirportsController(IAirportService airportService)
    {
        this._airportService = airportService;
    }
    [HttpGet("all")]
    public async Task<IActionResult> GetAll()
    {
        var allAirports = await this._airportService.GetAll().ToListAsync();
        return Ok(allAirports);
    }
    [HttpGet("{code}")]
    public async Task<IActionResult> GetByCode(string code)
    {
        if (String.IsNullOrEmpty(code))
        {
            return BadRequest("Invalid airport code format");
        }

        var airport = await this._airportService.GetAirportByCode(code);

        if (airport == null)
        {
            return NotFound($"Airport with specified code {code} does not exist");
        }

        return Ok(airport);
    }

    [HttpPost("create")]
    public async Task<IActionResult> Create([FromBody] AirportDTO airportDTO)
    {
        await this._airportService.AddAirport(airportDTO);
        return Created();
    }

    [HttpPut("edit")]
    public async Task<IActionResult> Edit([FromBody] AirportDTO airportDTO)
    {
        await this._airportService.UpdateAirport(airportDTO, airportDTO.Code);
        return Ok();
    }

    [HttpDelete("delete/{code}")]
    public async Task<IActionResult> Delete(string code)
    {
        if (String.IsNullOrEmpty(code))
        {
            return BadRequest("Invalid airport code format");
        }

        await this._airportService.DeleteAirport(code);
        return Ok();
    }
}
