using Airlines.API.Middlewares;
using Airlines.Business.Interfaces;
using Airlines.Business.WebServices;
using Airlines.Persistence.Basic.Context;
using Airlines.Persistence.Basic.Interfaces;
using Airlines.Persistence.Basic.Repositories;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddDbContext<AirlinesDbContext>((options =>
    options.UseSqlServer(builder.Configuration.GetConnectionString("Local"))))
                .AddScoped<IFlightRepository, FlightRepository>()
                .AddScoped<IAirlineRepository, AirlineRepository>()
                .AddScoped<IAirportRepository, AirportRepository>()
                .AddScoped<IFlightService, FlightService>()
                .AddScoped<IAirportService, AirportService>()
                .AddScoped<IAirlineService, AirlineService>();

builder.Services.AddControllers();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddCors();

var app = builder.Build();



// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.UseMiddleware<GlobalExceptionHandlingMiddleware>();

app.MapControllers();

app.Run();
