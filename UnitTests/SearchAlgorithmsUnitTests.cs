﻿using System;
using Airlines.Business.Exceptions;
using Airlines.Business.Facilities;
using Airlines.Business.Utils;
using Xunit;

namespace Airlines.Tests
{
    public class SearcherTests
    {
        [Fact]
        public void IdentifyMatch_ShouldReturnAirportWhenSearchTermMatchesAirport()
        {
            // Arrange
            var searchTerm = "JFK";
            var airports = new Airport[]
            {
                new Airport("JFK", "John F. Kennedy International Airport", "New York", "USA"),
                new Airport("LAX", "Los Angeles International Airport", "Los Angeles", "USA"),
                // Add more airports as needed for testing
            };
            var airlines = new Airline[] { };
            var flights = new Flight[] { };

            // Act
            var result = Searcher.IdentifyMatch(airports, airlines, flights, searchTerm);

            // Assert
            Assert.Equal("Airport", result);
        }

        [Fact]
        public void IdentifyMatch_ShouldReturnAirlineWhenSearchTermMatchesAirline()
        {
            // Arrange
            var searchTerm = "Delta";
            var airports = new Airport[] { };
            var airlines = new Airline[]
            {
                new Airline("Delta"),
                new Airline("Ameri"),
                // Add more airlines as needed for testing
            };
            var flights = new Flight[] { };

            // Act
            var result = Searcher.IdentifyMatch(airports, airlines, flights, searchTerm);

            // Assert
            Assert.Equal("Airline", result);
        }

        [Fact]
        public void IdentifyMatch_ShouldReturnFlightWhenSearchTermMatchesFlight()
        {
            // Arrange
            var searchTerm = "ABC123";
            var airports = new Airport[] { };
            var airlines = new Airline[] { };
            Aircraft aircraft1 = new Aircraft("aeaead");
            Aircraft aircraft2 = new Aircraft("deadwg");
            var flights = new Flight[]
            {
                new Flight("ABC123","ada","adadaw", aircraft1, "23","53"),
                new Flight("XYZ456", "zdzzd", "agrrg", aircraft2, "12", "21"),
                // Add more flights as needed for testing
            };

            // Act
            var result = Searcher.IdentifyMatch(airports, airlines, flights, searchTerm);

            // Assert
            Assert.Equal("Flight", result);
        }

        [Fact]
        public void IdentifyMatch_ShouldThrowExceptionWhenSearchTermDoesNotMatchAnyFacility()
        {
            // Arrange
            var searchTerm = "Nonexistent";
            var airports = new Airport[] { };
            var airlines = new Airline[] { };
            var flights = new Flight[] { };

            // Act & Assert
            Assert.Throws<NotFoundException>(() => Searcher.IdentifyMatch(airports, airlines, flights, searchTerm));
        }

        [Fact]
        public void LinearSearch_ShouldReturnCorrectIndex_ForExistingItem()
        {
            // Arrange
            string[] arr = { "Apple", "Banana", "Cherry", "Date" };

            // Act
            int result = Searcher.LinearSearch(arr, "Banana");

            // Assert
            Assert.Equal(1, result);
        }

        [Fact]
        public void LinearSearch_ShouldReturnNegativeOne_ForNonExistingItem()
        {
            // Arrange
            string[] arr = { "Apple", "Banana", "Cherry", "Date" };

            // Act
            int result = Searcher.LinearSearch(arr, "Grape");

            // Assert
            Assert.Equal(-1, result);
        }

        [Fact]
        public void BinarySearch_ShouldReturnCorrectIndex_ForExistingItem()
        {
            // Arrange
            string[] arr = { "Apple", "Banana", "Cherry", "Date", "Fig", "Grape" };

            // Act
            int result = Searcher.BinarySearch(arr, "Cherry");

            // Assert
            Assert.Equal(2, result);
        }

        [Fact]
        public void BinarySearch_ShouldReturnNegativeOne_ForNonExistingItem()
        {
            // Arrange
            string[] arr = { "Apple", "Banana", "Cherry", "Date", "Fig", "Grape" };

            // Act
            int result = Searcher.BinarySearch(arr, "Kiwi");

            // Assert
            Assert.Equal(-1, result);
        }

        [Fact]
        public void BinarySearch_ShouldReturnCorrectIndex_ForExistingItemInOddLengthArray()
        {
            // Arrange
            string[] arr = { "Apple", "Banana", "Cherry", "Date", "Fig", "Grape", "Kiwi" };

            // Act
            int result = Searcher.BinarySearch(arr, "Date");

            // Assert
            Assert.Equal(3, result);
        }

        [Fact]
        public void BinarySearch_ShouldReturnNegativeOne_ForNonExistingItemInOddLengthArray()
        {
            // Arrange
            string[] arr = { "Apple", "Banana", "Cherry", "Date", "Fig", "Grape", "Kiwi" };

            // Act
            int result = Searcher.BinarySearch(arr, "Orange");

            // Assert
            Assert.Equal(-1, result);
        }
    }
}
