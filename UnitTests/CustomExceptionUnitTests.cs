﻿using Airlines.Business.DatabaseNS;
using Airlines.Business.Exceptions;
using Airlines.Business.Facilities;
using Airlines.Business.Services;

namespace Airlines.Tests;
public class CustomExceptionUnitTests
{
    [Fact]
    public void AddFlight_WithInvalidAircraftModel_ThrowsInvalidInputException()
    {
        // Act and Assert
        Assert.Throws<InvalidInputException>(() => FlightService.AddFlight("FL123", "JFK", "LAX", "InvalidModel", "23", "12"));
    }

    [Fact]
    public void PerformSearch_NonExistingEntity_ThrowsNotFoundException()
    {
        // Arrange
        Database db = Database.GetInstance();
        // Act & Assert
        Assert.Throws<NotFoundException>(() => db.PerformSearch("NonExistingEntity"));
    }

    [Fact]
    public void AddFlight_FlightAlreadyExists_ThrowsRecordAlreadyExistsException()
    {
        // Arrange
        var aircraft = new PassengerAircraft("Model2", "100", "200", "150");
        var airport = new Airport("SEL", "DepartureAirport", "City DEF", "Country DEF");
        var airport2 = new Airport("KAL", "adwa", "City DEawdaF", "Countrwdaawy DEF");
        var flight = new Flight("F002", "SEL", "KAL", aircraft, "23", "23");
        Database db = Database.GetInstance();
        db.AddAirport(airport);
        db.AddAirport(airport2);
        db.AddFlight(flight);

        // Act & Assert
        Assert.Throws<RecordAlreadyExistsException>(() => db.AddFlight(flight));
    }

}
