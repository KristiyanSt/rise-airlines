﻿using Airlines.Business;
using Airlines.Business.DatabaseNS;
using Airlines.Business.DataStructures;
using Airlines.Business.Facilities;
using Airlines.Business.Services;
using Airlines.Business.Strategies;
using System.Windows.Input;

namespace Airlines.Tests;
public class FlightNetworkUnitTests
{
    private string airportFileName = "../../../StaticDataResources/Airport.csv";
    private string airlineFileName = "../../../StaticDataResources/Airline.csv";
    private string aircraftFileName = "../../../StaticDataResources/Aircraft.csv";
    private string flightFileName = "../../../StaticDataResources/Flight.csv";
    private string routeFilename = "../../../StaticDataResources/Route.csv";
    private string invalidRouteFilename = "../../../StaticDataResources/InvalidRoute.csv";

    [Fact]
    public void FlightNetwork_ShouldNotBeNull()
    {
        Database.Reset();
        FileDataHandler fh = new FileDataHandler();
        fh.ReadAndCollectFileData(this.airportFileName, "airport");
        fh.ReadAndCollectFileData(this.airlineFileName, "airline");
        fh.ReadAndCollectFileData(this.aircraftFileName, "aircraft");
        fh.ReadAndCollectFileData(this.flightFileName, "flight");
        fh.ReadAndCollectFileData(this.routeFilename, "route");

        ShortestRouteStrategy srs = new ShortestRouteStrategy();
        FlightNetwork fn = new FlightNetwork(srs);
        fn.PopulateNetwork();
        Route? connectionRoute = fn.ExecuteStrategy("JFK", "ATL");
        Assert.NotNull(connectionRoute);
    }
    [Fact]
    public void FlightNetwork_ShouldCreateCorrectRouteFlightsCount_One()
    {
        Database.Reset();
        FileDataHandler fh = new FileDataHandler();
        fh.ReadAndCollectFileData(this.airportFileName, "airport");
        fh.ReadAndCollectFileData(this.airlineFileName, "airline");
        fh.ReadAndCollectFileData(this.aircraftFileName, "aircraft");
        fh.ReadAndCollectFileData(this.flightFileName, "flight");
        fh.ReadAndCollectFileData(this.routeFilename, "route");

        ShortestRouteStrategy srs = new ShortestRouteStrategy();
        FlightNetwork fn = new FlightNetwork(srs);
        fn.PopulateNetwork();
        Route? connectionRoute = fn.ExecuteStrategy("JFK", "ATL");
        Assert.Equal(connectionRoute.GetCount(), (uint)3);
    }

    [Fact]
    public void FlightNetwork_ShouldCreateCorrectRouteFlightsCount_Two()
    {
        Database.Reset();
        FileDataHandler fh = new FileDataHandler();
        fh.ReadAndCollectFileData(this.airportFileName, "airport");
        fh.ReadAndCollectFileData(this.airlineFileName, "airline");
        fh.ReadAndCollectFileData(this.aircraftFileName, "aircraft");
        fh.ReadAndCollectFileData(this.flightFileName, "flight");
        fh.ReadAndCollectFileData(this.routeFilename, "route");

        ShortestRouteStrategy srs = new ShortestRouteStrategy();
        FlightNetwork fn = new FlightNetwork(srs);
        fn.PopulateNetwork();
        Route? connectionRoute = fn.ExecuteStrategy("SFO", "MIA");
        Assert.Equal(connectionRoute.GetCount(), (uint)1);
    }

    [Fact]
    public void FlightNetwork_ShouldNotCreateRoute_WithNoConnectionBetweenAirports()
    {
        Database.Reset();
        FileDataHandler fh = new FileDataHandler();
        fh.ReadAndCollectFileData(this.airportFileName, "airport");
        fh.ReadAndCollectFileData(this.airlineFileName, "airline");
        fh.ReadAndCollectFileData(this.aircraftFileName, "aircraft");
        fh.ReadAndCollectFileData(this.flightFileName, "flight");
        fh.ReadAndCollectFileData(this.routeFilename, "route");

        ShortestRouteStrategy srs = new ShortestRouteStrategy();
        FlightNetwork fn = new FlightNetwork(srs);
        fn.PopulateNetwork();
        Route? connectionRoute = fn.ExecuteStrategy("MIA", "BOS");
        Assert.Null(connectionRoute);
    }

    [Fact]
    public void FlightNetwork_RouteListShouldBeCorrect()
    {
        Database.Reset();
        FileDataHandler fh = new FileDataHandler();
        fh.ReadAndCollectFileData(this.airportFileName, "airport");
        fh.ReadAndCollectFileData(this.airlineFileName, "airline");
        fh.ReadAndCollectFileData(this.aircraftFileName, "aircraft");
        fh.ReadAndCollectFileData(this.flightFileName, "flight");
        fh.ReadAndCollectFileData(this.routeFilename, "route");

        ShortestRouteStrategy srs = new ShortestRouteStrategy();
        FlightNetwork fn = new FlightNetwork(srs);
        fn.PopulateNetwork();
        Route? connectionRoute = fn.ExecuteStrategy("BOS", "ORD");
        Assert.Equal(connectionRoute.ToStringList(), new List<string>() { "FL900", "FL123", "FL456" });
    }

    [Fact]
    public void FlightShouldContainPriceAndTime()
    {
        Database.Reset();
        FileDataHandler fh = new FileDataHandler();
        fh.ReadAndCollectFileData(this.airportFileName, "airport");
        fh.ReadAndCollectFileData(this.airlineFileName, "airline");
        fh.ReadAndCollectFileData(this.aircraftFileName, "aircraft");
        fh.ReadAndCollectFileData(this.flightFileName, "flight");
        fh.ReadAndCollectFileData(this.routeFilename, "route");

        Flight current = FlightService.GetFlightById("FL123");
        Assert.NotNull(current.Price);
        Assert.NotNull(current.Time);
    }

    [Fact]
    public void FlightNetwork_ShortestRoute_ShouldBuildDirectFlight()
    {
        Database.Reset();
        FileDataHandler fh = new FileDataHandler();
        fh.ReadAndCollectFileData(this.airportFileName, "airport");
        fh.ReadAndCollectFileData(this.airlineFileName, "airline");
        fh.ReadAndCollectFileData(this.aircraftFileName, "aircraft");
        fh.ReadAndCollectFileData(this.flightFileName, "flight");
        fh.ReadAndCollectFileData(this.routeFilename, "route");

        ShortestRouteStrategy srs = new ShortestRouteStrategy();
        FlightNetwork fn = new FlightNetwork(srs);
        fn.PopulateNetwork();
        Route? connectionRoute = fn.ExecuteStrategy("BOS", "JFK");
        Assert.Equal(connectionRoute.ToStringList(), new List<string>() { "FL900" });
    }

    [Fact]
    public void FlightNetwork_LeastPriceRoute_ShouldBuildLongerButCheaperRoute()
    {
        Database.Reset();
        FileDataHandler fh = new FileDataHandler();
        fh.ReadAndCollectFileData(this.airportFileName, "airport");
        fh.ReadAndCollectFileData(this.airlineFileName, "airline");
        fh.ReadAndCollectFileData(this.aircraftFileName, "aircraft");
        fh.ReadAndCollectFileData(this.flightFileName, "flight");
        fh.ReadAndCollectFileData(this.routeFilename, "route");

        LeastPriceStrategy lps = new LeastPriceStrategy();
        FlightNetwork fn = new FlightNetwork(lps);
        fn.PopulateNetwork();
        Route? connectionRoute = fn.ExecuteStrategy("BOS", "JFK");
        Assert.Equal(connectionRoute.ToStringList(), new List<string>() { "FL303", "FL404", "FL505" });
    }

    [Fact]
    public void FlightNetwork_LeastTimeRoute_ShouldBuildLeastTimeRoute()
    {
        Database.Reset();
        FileDataHandler fh = new FileDataHandler();
        fh.ReadAndCollectFileData(this.airportFileName, "airport");
        fh.ReadAndCollectFileData(this.airlineFileName, "airline");
        fh.ReadAndCollectFileData(this.aircraftFileName, "aircraft");
        fh.ReadAndCollectFileData(this.flightFileName, "flight");
        fh.ReadAndCollectFileData(this.routeFilename, "route");

        LeastTimeStrategy lts = new LeastTimeStrategy();
        FlightNetwork fn = new FlightNetwork(lts);
        fn.PopulateNetwork();
        Route? connectionRoute = fn.ExecuteStrategy("SEA", "JFK");
        Assert.Equal(connectionRoute.ToStringList(), new List<string>() { "FL404", "FL505" });
    }

    [Fact]
    public void FlightNetwork_LeastTimeRoute_ShouldNotBuildRoute()
    {
        Database.Reset();
        FileDataHandler fh = new FileDataHandler();
        fh.ReadAndCollectFileData(this.airportFileName, "airport");
        fh.ReadAndCollectFileData(this.airlineFileName, "airline");
        fh.ReadAndCollectFileData(this.aircraftFileName, "aircraft");
        fh.ReadAndCollectFileData(this.flightFileName, "flight");
        fh.ReadAndCollectFileData(this.routeFilename, "route");

        LeastTimeStrategy lts = new LeastTimeStrategy();
        FlightNetwork fn = new FlightNetwork(lts);
        fn.PopulateNetwork();
        Route? connectionRoute = fn.ExecuteStrategy("SEA", "DCA");
        Assert.Equal(connectionRoute.GetCount(), (uint)0);
    }
}
