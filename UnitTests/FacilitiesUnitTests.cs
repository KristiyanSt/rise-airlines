﻿using Airlines.Business.Facilities;
using Airlines.Business.Services;
using System.Diagnostics.Metrics;
using Airlines.Business.DatabaseNS;
using Airlines.Business.Exceptions;

namespace Airlines.Tests;
public class FacilitiesUnitTests
{

    [Fact]
    public void AddAirline_ValidInput_Success()
    {
        // Arrange
        string airlineName = "Airli";

        // Act
        Airline result = AirlineService.AddAirline(airlineName);

        // Assert
        Assert.True(result.GetType() == typeof(Airline));
        // You might also want to check if the airline was added to the database or some other confirmation mechanism.
    }

    [Fact]
    public void GetAirlines_NonEmptyDatabase_ReturnsAirlines()
    {
        // Arrange
        AirlineService.AddAirline("Aira");
        AirlineService.AddAirline("Airg");
        AirlineService.AddAirline("Airn");

        // Act
        var airlines = Database.GetInstance().GetAirlines();

        // Assert
        Assert.NotEmpty(airlines);
        // You might want to further verify the contents of the list or check specific airline properties.
    }

    [Fact]
    public void AddAirport_ValidInput_Success()
    {
        // Arrange
        string identifier = "GOH";
        string name = "Test Airport";
        string city = "Test City";
        string country = "Test Country";

        // Act
        var result = AirportService.AddAirport(identifier, name, city, country);

        // Assert
        Assert.True(result.City == city);
        // You might also want to check if the airport was added to the database or some other confirmation mechanism.
    }

    [Fact]
    public void GetAirports_EmptyDatabase_ReturnsNotEmptyList()
    {
        // Arrange

        // Act
        var airports = Database.GetInstance().GetAirports();

        // Assert
        Assert.NotEmpty(airports);
    }

    [Fact]
    public void GetAirports_NonEmptyDatabase_ReturnsAirports()
    {
        // Arrange
        AirportService.AddAirport("ABC", "Airportf", "Cityj", "Countryh");
        AirportService.AddAirport("MDK", "Airportd", "Cityh", "Countryf");
        AirportService.AddAirport("GHI", "Airports", "Cityg", "Countrys");

        // Act
        var airports = Database.GetInstance().GetAirports();

        // Assert
        Assert.NotEmpty(airports);
        // You might want to further verify the contents of the list or check specific airport properties.
    }

    [Fact]
    public void AddFlight_ValidInput_Success()
    {
        // Arrange
        Database.GetInstance().AddCargoAircraft(new CargoAircraft("Boeing 837", "1", "3"));
        string flightId = "FLKA5";
        Database.GetInstance().AddAirport(new Airport("SKA", "ADA", "GRG", "FKEJ"));
        string departureAirport = "SKA";
        AirportService.AddAirport("OOP", "Airport", "City", "Country");
        string arrivalAirport = "OOP";

        // Act
        var result = FlightService.AddFlight(flightId, departureAirport, arrivalAirport, "Boeing 837", "33", "21");

        // Assert
        Assert.True(result.Identifier == flightId);
        // You might also want to check if the flight was added to the database or some other confirmation mechanism.
    }


    [Fact]
    public void GetFlights_NonEmptyDatabase_ReturnsFlights()
    {
        // Arrange
        CargoAircraft ca = new CargoAircraft("Boeing 737", "4", "7");
        CargoAircraft ca2 = new CargoAircraft("Airbus A320", "4", "7");
        Database.Reset();
        Database.GetInstance().AddCargoAircraft(ca);
        Database.GetInstance().AddCargoAircraft(ca2);
        Airport ai = new Airport("DEF", "DAW", "ADWA", "DAAD");
        Airport ai2 = new Airport("SAD", "DAW", "ADWA", "DAAD");
        Database.GetInstance().AddAirport(ai);
        Database.GetInstance().AddAirport(ai2);
        FlightService.AddFlight("FL456", "SAD", "DEF", "Airbus A320", "23", "53");

        // Act
        var flights = Database.GetInstance().GetFlights();

        // Assert
        Assert.NotEmpty(flights);
        // You might want to further verify the contents of the list or check specific flight properties.
    }

    [Fact]
    public void AppendFlight_AddsFlightToRoute()
    {
        // Arrange
        var route = new Route();
        PassengerAircraft pa = new PassengerAircraft("Model1", "13", "13", "13");
        PassengerAircraft pa2 = new PassengerAircraft("Model2", "23", "23", "23");
        var flight1 = new Flight("F123", "ABC", "DEF", pa, "23", "83");
        var flight2 = new Flight("F124", "DEF", "GHI", pa2, "13", "53");

        // Act
        route.AppendFlight(flight1);
        route.AppendFlight(flight2);

        // Assert
        Assert.Equal((uint)2, route.GetCount());
        // You might also want to verify if the flights were added in the correct order.
    }

    [Fact]
    public void RemoveLastFlight_RemovesLastFlightFromRoute()
    {
        // Arrange
        var route = new Route();
        PassengerAircraft pa = new PassengerAircraft("Model1", "13", "13", "13");
        PassengerAircraft pa2 = new PassengerAircraft("Model2", "23", "23", "23");
        var flight1 = new Flight("F123", "ABC", "DEF", pa, "23", "53");
        var flight2 = new Flight("F124", "DEF", "GHI", pa2, "29", "51");
        route.AppendFlight(flight1);
        route.AppendFlight(flight2);

        // Act
        route.RemoveLastFlight();

        // Assert
        Assert.Equal((uint)1, route.GetCount());
        // You might also want to verify if the correct flight was removed.
    }

    [Fact]
    public void AppendFlight_InvalidFlightSequence_ThrowsException()
    {
        // Arrange
        var route = new Route();
        PassengerAircraft pa = new PassengerAircraft("Models", "13", "13", "13");
        PassengerAircraft pa2 = new PassengerAircraft("Modelf", "23", "23", "23");
        var flight1 = new Flight("F123", "ABC", "DEF", pa, "23", "53");
        var flight2 = new Flight("F124", "ABC", "GHI", pa2, "31", "92");
        route.AppendFlight(flight1);
        // Act & Assert
        Assert.Throws<InvalidInputException>(() => route.AppendFlight(flight2));
        // You might want to further verify the exception message or if the route count remains unchanged.
    }
}
