﻿using Airlines.Business.Utils;
using System.Diagnostics;
using Xunit.Abstractions;

namespace Airlines.Tests
{
    public class PerformanceTests
    {
        private const int _arrayLengthSeed = 6;
        private const int _stringsLengthSeed = 12;
        private const int _oneMinuteInMs = 1 * 6;
        private Random _random = new Random();
        private ITestOutputHelper _outputHelper;
        public PerformanceTests(ITestOutputHelper outputHelper)
        {
            this._outputHelper = outputHelper;
        }
        private string GenerateRandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            char[] stringChars = new char[length];
            for (int i = 0; i < length; i++)
            {
                int randomIndex = GenerateRandomIndex(chars.Length);
                stringChars[i] = chars[randomIndex];
            }
            return new string(stringChars);
        }
        private string[] GenerateRandomStringArray(int arrLength, int stringsLength)
        {
            string[] array = new string[arrLength];
            for (int i = 0; i < arrLength; i++)
            {
                string randomString = GenerateRandomString(stringsLength);
                array[i] = randomString;
            }
            return array;
        }
        private int GenerateRandomIndex(int arrayLength)
        {
            return _random.Next(0, arrayLength);
        }

        [Fact]
        public void BubbleSort_PerformanceTest()
        {
            Stopwatch stopwatch = new Stopwatch();
            int sortIterations = 0;
            stopwatch.Start();
            while (stopwatch.ElapsedMilliseconds <= _oneMinuteInMs)
            {
                string[] randomArray = GenerateRandomStringArray(_arrayLengthSeed, _stringsLengthSeed);
                randomArray.BubbleSort("ascending");
                sortIterations += 1;
            }
            stopwatch.Stop();
            _outputHelper.WriteLine($"Bubble sort performed {sortIterations} sort iterations in one minute");
        }

        [Fact]
        public void SelectionSort_PerformanceTest()
        {
            Stopwatch stopwatch = new Stopwatch();
            int sortIterations = 0;
            stopwatch.Start();
            while (stopwatch.ElapsedMilliseconds <= _oneMinuteInMs)
            {
                string[] randomArray = GenerateRandomStringArray(_arrayLengthSeed, _stringsLengthSeed);
                randomArray.SelectionSort("descending");
                sortIterations += 1;
            }
            stopwatch.Stop();
            _outputHelper.WriteLine($"Selection sort performed {sortIterations} sort iterations in one minute");
        }

        [Fact]
        public void LinearSearch_PerformanceTest()
        {
            Stopwatch stopwatch = new Stopwatch();
            int searchIterations = 0;
            string[] randomArray = GenerateRandomStringArray(_arrayLengthSeed, _stringsLengthSeed);
            stopwatch.Start();

            while (stopwatch.ElapsedMilliseconds <= _oneMinuteInMs)
            {
                int randomIndex = GenerateRandomIndex(randomArray.Length);
                Searcher.LinearSearch(randomArray, randomArray[randomIndex]);
                searchIterations += 1;
            }
            stopwatch.Stop();
            _outputHelper.WriteLine($"Linear search performed {searchIterations} search iterations in one minute");
        }

        [Fact]
        public void BinarySearch_PerformanceTest()
        {
            Stopwatch stopwatch = new Stopwatch();
            int searchIterations = 0;
            string[] randomArray = GenerateRandomStringArray(_arrayLengthSeed, _stringsLengthSeed);

            //sort the random array before performing binary search
            Array.Sort(randomArray);

            stopwatch.Start();
            while (stopwatch.ElapsedMilliseconds <= _oneMinuteInMs)
            {
                int randomIndex = GenerateRandomIndex(randomArray.Length);
                Searcher.BinarySearch(randomArray, randomArray[randomIndex]);
                searchIterations += 1;
            }
            stopwatch.Stop();
            _outputHelper.WriteLine($"Binary search performed {searchIterations} search iterations in one minute");
        }
    }
}
