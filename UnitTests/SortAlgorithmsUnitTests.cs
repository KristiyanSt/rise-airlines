﻿using Airlines.Business.Utils;

namespace Airlines.Tests
{
    public class SortAlgorithmsUnitTests
    {
        [Theory]
        [InlineData(
            new string[] { "air", "bir", "hir", "oar" },
            new string[] { "hir", "oar", "bir", "air" },
            "ascending"
            )]
        [InlineData(
            new string[] { "air", "bir", "hir", "hir", "oar" },
            new string[] { "hir", "hir", "oar", "bir", "air" },
            "ascending"
            )]
        [InlineData(
            new string[] { "oare", "hire", "bire", "bire", "aire" },
            new string[] { "aire", "bire", "hire", "oare", "bire" },
            "descending"
            )]
        [InlineData(
            new string[] { "smea", "que", "flea", "crea", "aero" },
            new string[] { "aero", "que", "smea", "flea", "crea" },
            "descending"
            )]
        [InlineData(
            new string[0],
          new string[0],
            "descending"
            )]
        [InlineData(
            new string[0],
          new string[0],
            "ascending"
            )]
        public void ValidateSelectionSort_ShouldBeEqual(string[] sortedArray, string[] unsortedArray, string orderBy)
        {
            //act
            unsortedArray.SelectionSort(orderBy);
            //assert
            Assert.Equal(sortedArray, unsortedArray);
        }

        [Theory]
        [InlineData(
            new string[] { "air", "bir", "hir", "oar" },
            new string[] { "hir", "oar", "bir", "air" },
            "ascending"
            )]
        [InlineData(
            new string[] { "air", "bir", "hir", "hir", "oar" },
            new string[] { "hir", "hir", "oar", "bir", "air" },
            "ascending"
            )]
        [InlineData(
            new string[] { "oare", "hire", "bire", "bire", "aire" },
            new string[] { "aire", "bire", "hire", "oare", "bire" },
            "descending"
            )]
        [InlineData(
            new string[] { "smea", "que", "flea", "crea", "aero" },
            new string[] { "aero", "que", "smea", "flea", "crea" },
            "descending"
            )]
        [InlineData(
            new string[0],
          new string[0],
            "descending"
            )]
        [InlineData(
            new string[0],
          new string[0],
            "ascending"
            )]
        public void ValidateBubbleSort_ShouldBeEqual(string[] sortedArray, string[] unsortedArray, string orderBy)
        {
            //act
            unsortedArray.BubbleSort(orderBy);
            //assert
            Assert.Equal(sortedArray, unsortedArray);
        }

        [Fact]
        public void DefaultSort_ShouldSortListInAscendingOrder()
        {
            // Arrange
            var list = new List<string> { "banana", "apple", "grape", "orange" };
            var expected = new List<string> { "apple", "banana", "grape", "orange" };

            // Act
            var sortedList = list.DefaultSort();

            // Assert
            Assert.Equal(expected, sortedList);
        }

        [Fact]
        public void DescendingSort_ShouldSortListInDescendingOrder()
        {
            // Arrange
            var list = new List<string> { "banana", "apple", "grape", "orange" };
            var expected = new List<string> { "orange", "grape", "banana", "apple" };

            // Act
            var sortedList = list.DescendingSort();

            // Assert
            Assert.Equal(expected, sortedList);
        }

        [Fact]
        public void DefaultSort_ShouldHandleEmptyList()
        {
            // Arrange
            var list = new List<string>();

            // Act
            var sortedList = list.DefaultSort();

            // Assert
            Assert.Empty(sortedList);
        }

        [Fact]
        public void DescendingSort_ShouldHandleEmptyList()
        {
            // Arrange
            var list = new List<string>();

            // Act
            var sortedList = list.DescendingSort();

            // Assert
            Assert.Empty(sortedList);
        }
    }
}
