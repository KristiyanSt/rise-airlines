﻿using Airlines.Business.DatabaseNS;
using Airlines.Business;
using Airlines.Business.Facilities;
using Airlines.Business.DataStructures;

namespace Airlines.Tests;
public class FindRouteTests
{
    private string airportFileName = "../../../StaticDataResources/Airport.csv";
    private string airlineFileName = "../../../StaticDataResources/Airline.csv";
    private string aircraftFileName = "../../../StaticDataResources/Aircraft.csv";
    private string flightFileName = "../../../StaticDataResources/Flight.csv";
    private string routeFilename = "../../../StaticDataResources/Route.csv";
    private string invalidRouteFilename = "../../../StaticDataResources/InvalidRoute.csv";

    [Fact]
    public void LoadFlightRouteData_RootStartingAirportShouldBeCorrect()
    {
        Database db = Database.Reset();
        FileDataHandler fh = new FileDataHandler();

        fh.ReadAndCollectFileData(this.airportFileName, "airport");
        fh.ReadAndCollectFileData(this.routeFilename, "route");

        Assert.Equal("DFW", db.GetFlightRoutesTree()!.Root.Airport.Identifier);
    }

    [Fact]
    public void LoadFlightRouteData_FlightRoutesTreeShouldNotBeNull()
    {
        Database db = Database.Reset();
        FileDataHandler fh = new FileDataHandler();

        fh.ReadAndCollectFileData(this.airportFileName, "airport");
        fh.ReadAndCollectFileData(this.routeFilename, "route");

        Assert.NotNull(db.GetFlightRoutesTree());
    }

    [Fact]
    public void LoadFlightRouteData_RootChildrenShouldNotBeNull()
    {
        Database db = Database.Reset();
        FileDataHandler fh = new FileDataHandler();


        fh.ReadAndCollectFileData(this.airportFileName, "airport");
        fh.ReadAndCollectFileData(this.routeFilename, "route");

        Assert.NotNull(db.GetFlightRoutesTree()!.Root.Children);
    }

    [Fact]
    public void LoadInvalidFlightRouteData_RootChildrenShouldNotBeNull()
    {
        Database db = Database.Reset();
        FileDataHandler fh = new FileDataHandler();

        fh.ReadAndCollectFileData(this.airportFileName, "airport");
        fh.ReadAndCollectFileData(this.invalidRouteFilename, "route");

        Assert.Equal(db.GetFlightRoutesTree()!.Root.Children.Count, 0);
    }

    [Fact]
    public void SearchRouteCommand_ShouldBeDirect()
    {
        Database db = Database.Reset();
        FileDataHandler fh = new FileDataHandler();
        fh.ReadAndCollectFileData(this.airportFileName, "airport");
        fh.ReadAndCollectFileData(this.airlineFileName, "airline");
        fh.ReadAndCollectFileData(this.aircraftFileName, "aircraft");
        fh.ReadAndCollectFileData(this.flightFileName, "flight");
        fh.ReadAndCollectFileData(this.routeFilename, "route");

        Tree flightRoutesTree = db.GetFlightRoutesTree()!;
        string destinationAirport = "JFK";

        Route flightRoute = flightRoutesTree.SearchForRoute(destinationAirport, flightRoutesTree.Root)!;


        Assert.Equal(flightRoute.ToStringList(), new List<string> { "FL505" });
    }

    [Fact]
    public void SearchRouteCommand_ShouldBeIndirectWithTwoFlights()
    {
        Database db = Database.Reset();
        FileDataHandler fh = new FileDataHandler();
        fh.ReadAndCollectFileData(this.airportFileName, "airport");
        fh.ReadAndCollectFileData(this.airlineFileName, "airline");
        fh.ReadAndCollectFileData(this.aircraftFileName, "aircraft");
        fh.ReadAndCollectFileData(this.flightFileName, "flight");
        fh.ReadAndCollectFileData(this.routeFilename, "route");

        Tree flightRoutesTree = db.GetFlightRoutesTree()!;
        string destinationAirport = "LAX";

        Route flightRoute = flightRoutesTree.SearchForRoute(destinationAirport, flightRoutesTree.Root)!;


        Assert.Equal(flightRoute.ToStringList(), new List<string> { "FL505", "FL123" });
    }
    [Fact]
    public void SearchRouteCommand_ShouldBeIndirectWithThreeFlights()
    {
        Database db = Database.Reset();
        FileDataHandler fh = new FileDataHandler();
        fh.ReadAndCollectFileData(this.airportFileName, "airport");
        fh.ReadAndCollectFileData(this.airlineFileName, "airline");
        fh.ReadAndCollectFileData(this.aircraftFileName, "aircraft");
        fh.ReadAndCollectFileData(this.flightFileName, "flight");
        fh.ReadAndCollectFileData(this.routeFilename, "route");

        Tree flightRoutesTree = db.GetFlightRoutesTree()!;
        string destinationAirport = "ORD";

        Route flightRoute = flightRoutesTree.SearchForRoute(destinationAirport, flightRoutesTree.Root)!;


        Assert.Equal(flightRoute.ToStringList(), new List<string> { "FL505", "FL123", "FL456" });
    }

}
