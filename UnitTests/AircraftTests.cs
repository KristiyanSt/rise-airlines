﻿using Airlines.Business.DatabaseNS;
using Airlines.Business.Facilities;
using Airlines.Business.Services;

namespace Airlines.Tests;
public class AircraftTests
{
    [Fact]
    public void AddCargoAircraft_ValidInput_Success()
    {
        // Arrange
        string model = "MSAWEA";
        string cargoCapacity = "1000";
        string cargoVolume = "2000";

        // Act
        CargoAircraft result = AircraftService.AddCargoAircraft(model, cargoCapacity, cargoVolume);

        // Assert
        Assert.True(result.Model == model);
        // You might also want to check if the aircraft was added to the database or some other confirmation mechanism.
    }

    [Fact]
    public void AddPassengerAircraft_ValidInput_Success()
    {
        // Arrange
        string model = "IGDWYVB";
        string cargoCapacity = "500";
        string cargoVolume = "1000";
        string seats = "200";

        // Act
        PassengerAircraft result = AircraftService.AddPassengerAircraft(model, cargoCapacity, cargoVolume, seats);

        // Assert
        Assert.True(result.Model == model);
        // You might also want to check if the aircraft was added to the database or some other confirmation mechanism.
    }

    [Fact]
    public void AddPrivateAircraft_ValidInput_Success()
    {
        // Arrange
        string model = "Model3";
        string seats = "10";

        // Act
        PrivateAircraft result = AircraftService.AddPrivateAircraft(model, seats);

        // Assert
        Assert.True(result.Model == model);
        // You might also want to check if the aircraft was added to the database or some other confirmation mechanism.
    }
}
