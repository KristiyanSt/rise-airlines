﻿using Airlines.Business.DatabaseNS;
using Airlines.Business.Exceptions;
using Airlines.Business.Facilities;

namespace Airlines.Tests;
public class DatabaseUnitTests
{
    private Database database;

    public DatabaseUnitTests()
    {
        database = Database.GetInstance();
    }
    [Fact]
    public void AddAirport_ValidAirport_SuccessfullyAdded()
    {
        // Arrange
        var airport = new Airport("AAC", "Airport ABC", "City ABC", "Country ABC");

        // Act
        var addedAirport = database.AddAirport(airport);

        // Assert
        Assert.Equal(airport, addedAirport);
        Assert.Contains(airport.Identifier.ToLower(), database.GetAirports().Keys);
    }

    [Fact]
    public void AddAirport_AirportAlreadyExists_ThrowsException()
    {
        // Arrange
        var airport = new Airport("KEA", "Airport DEF", "City DEF", "Country DEF");
        database.AddAirport(airport);

        // Act & Assert
        Assert.Throws<RecordAlreadyExistsException>(() => database.AddAirport(airport));
    }

    [Fact]
    public void AddAirline_ValidAirline_SuccessfullyAdded()
    {
        // Arrange
        var airline = new Airline("ABC s");

        // Act
        var addedAirline = database.AddAirline(airline);

        // Assert
        Assert.Equal(airline, addedAirline);
        Assert.Contains(airline.Name.ToLower(), database.GetAirlines().Keys);
    }

    [Fact]
    public void AddAirline_AirlineAlreadyExists_ThrowsException()
    {
        // Arrange
        var airline = new Airline("XYZ A");
        database.AddAirline(airline);

        // Act & Assert
        Assert.Throws<RecordAlreadyExistsException>(() => database.AddAirline(airline));
    }

    [Fact]
    public void AddFlight_ValidFlight_SuccessfullyAdded()
    {
        // Arrange
        var aircraft = new PassengerAircraft("Model1", "100", "200", "150");
        var airport = new Airport("KAM", "DepartureAirport", "City DEF", "Country DEF");
        var airport2 = new Airport("LAM", "adwa", "City DEawdaF", "Countrwdaawy DEF");
        database.AddAirport(airport);
        database.AddAirport(airport2);
        var flight = new Flight("F00O", "KAM", "LAM", aircraft, "23", "23");

        // Act
        var addedFlight = database.AddFlight(flight);

        // Assert
        Assert.Equal(flight, addedFlight);
        Assert.Contains(flight.Identifier.ToLower(), database.GetFlights().Keys);
    }

    [Fact]
    public void AddFlight_FlightAlreadyExists_ThrowsException()
    {
        // Arrange
        var aircraft = new PassengerAircraft("Model2", "100", "200", "150");
        var airport = new Airport("MAN", "DepartureAirport", "City DEF", "Country DEF");
        var airport2 = new Airport("GET", "adwa", "City DEawdaF", "Countrwdaawy DEF");
        database.AddAirport(airport);
        database.AddAirport(airport2);
        var flight = new Flight("F006", "MAN", "GET", aircraft, "23", "12");
        database.AddFlight(flight);

        // Act & Assert
        Assert.Throws<RecordAlreadyExistsException>(() => database.AddFlight(flight));
    }

    [Fact]
    public void AddCargoAircraft_ValidAircraft_SuccessfullyAdded()
    {
        // Arrange
        var aircraft = new CargoAircraft("Model3", "500", "600");

        // Act
        var addedAircraft = database.AddCargoAircraft(aircraft);

        // Assert
        Assert.Equal(aircraft, addedAircraft);
        Assert.Contains(aircraft.Model.ToLower(), database.GetCargoAircrafts().Keys);
    }

    [Fact]
    public void AddPassengerAircraft_ValidAircraft_SuccessfullyAdded()
    {
        // Arrange
        var aircraft = new PassengerAircraft("Model4", "100", "200", "150");

        // Act
        var addedAircraft = database.AddPassengerAircraft(aircraft);

        // Assert
        Assert.Equal(aircraft, addedAircraft);
        Assert.Contains(aircraft.Model.ToLower(), database.GetPassengerAircrafts().Keys);
    }

    [Fact]
    public void AddPrivateAircraft_ValidAircraft_SuccessfullyAdded()
    {
        // Arrange
        var aircraft = new PrivateAircraft("Model5", "10");

        // Act
        var addedAircraft = database.AddPrivateAircraft(aircraft);

        // Assert
        Assert.Equal(aircraft, addedAircraft);
        Assert.Contains(aircraft.Model.ToLower(), database.GetPrivateAircrafts().Keys);
    }

    [Fact]
    public void IsAirlineExisting_ExistingAirline_ReturnsTrue()
    {
        // Arrange
        var airline = new Airline("Airai");
        database.AddAirline(airline);

        // Act
        var exists = database.IsAirlineExisting("Airai");

        // Assert
        Assert.True(exists);
    }

    [Fact]
    public void IsAirlineExisting_NonExistingAirline_ReturnsFalse()
    {
        // Act
        var exists = database.IsAirlineExisting("NonExistingAirline");

        // Assert
        Assert.False(exists);
    }
    [Fact]
    public void FindById_ExistingFlight_ReturnsFlight()
    {
        // Arrange
        var aircraft = new PassengerAircraft("Model1", "100", "200", "150");
        Airport airport1 = new Airport("ADA", "ADA", "DAW", "dawdaw");
        Airport airport2 = new Airport("DAWD", "ADA", "DAW", "dawdaw");
        database.AddAirport(airport1);
        database.AddAirport(airport2);
        var flight = new Flight("F001", "ADA", "DAWD", aircraft, "23", "23");
        database.AddFlight(flight);

        // Act
        var foundFlight = database.FindById("F001");

        // Assert
        Assert.Equal(flight, foundFlight);
    }

    [Fact]
    public void FindById_NonExistingFlight_ReturnsNull()
    {
        // Arrange

        // Act
        var foundFlight = database.FindById("NonExistingFlight");

        // Assert
        Assert.Null(foundFlight);
    }

    [Fact]
    public void FindAircraftByModel_ExistingAircraft_ReturnsAircraft()
    {
        // Arrange
        var aircraft = new CargoAircraft("Model1", "500", "600");
        database.AddCargoAircraft(aircraft);

        // Act
        var foundAircraft = database.FindAircraftByModel("Model1");

        // Assert
        Assert.Equal(aircraft, foundAircraft);
    }

    [Fact]
    public void FindAircraftByModel_NonExistingAircraft_ReturnsNull()
    {
        // Act
        var foundAircraft = database.FindAircraftByModel("NonExistingModel");

        // Assert
        Assert.Null(foundAircraft);
    }

    [Fact]
    public void PerformSearch_ExistingEntity_ReturnsCorrectType()
    {
        // Arrange
        var airport = new Airport("VFA", "Airport ABC", "City ABC", "Country ABC");
        database.AddAirport(airport);

        // Act
        var entityType = database.PerformSearch("VFA");

        // Assert
        Assert.Equal("Airport", entityType);
    }

    [Fact]
    public void PerformSearch_NonExistingEntity_ThrowsException()
    {
        // Arrange
        // Act & Assert
        Assert.Throws<NotFoundException>(() => database.PerformSearch("NonExistingEntity"));
    }

    [Fact]
    public void GetAirports_ReturnsAllAirports()
    {
        // Arrange
        var airport1 = new Airport("MNA", "Airport ABC", "City ABC", "Country ABC");
        var airport2 = new Airport("DAG", "Airport DEF", "City DEF", "Country DEF");
        database.AddAirport(airport1);
        database.AddAirport(airport2);

        // Act
        var airports = database.GetAirports();

        // Assert
        Assert.Contains(airport1.Identifier.ToLower(), airports.Keys);
        Assert.Contains(airport2.Identifier.ToLower(), airports.Keys);
    }

    [Fact]
    public void GetAirlines_ReturnsAllAirlines()
    {
        // Arrange
        var airline1 = new Airline("KAMEH");
        var airline2 = new Airline("JGIRM");
        database.AddAirline(airline1);
        database.AddAirline(airline2);

        // Act
        var airlines = database.GetAirlines();

        // Assert
        Assert.Contains(airline1.Name.ToLower(), airlines.Keys);
        Assert.Contains(airline2.Name.ToLower(), airlines.Keys);
    }

    [Fact]
    public void GetFlights_ReturnsAllFlights()
    {
        // Arrange
        var aircraft = new PassengerAircraft("Model1", "100", "200", "150");
        Airport airport1 = new Airport("LLL", "DAW", "DWA", "DAW");
        Airport airport2 = new Airport("PPP", "DAW", "DWA", "DAW");
        database.AddAirport(airport1);
        database.AddAirport(airport2);
        var flight1 = new Flight("F00P", "LLL", "PPP", aircraft, "23", "23");
        var flight2 = new Flight("F00L", "PPP", "LLL", aircraft, "53", "53");
        database.AddFlight(flight1);
        database.AddFlight(flight2);

        // Act
        var flights = database.GetFlights();

        // Assert
        Assert.Contains(flight1.Identifier.ToLower(), flights.Keys);
        Assert.Contains(flight2.Identifier.ToLower(), flights.Keys);
    }

    [Fact]
    public void GetCargoAircrafts_ReturnsAllCargoAircrafts()
    {
        // Arrange
        var aircraft1 = new CargoAircraft("FSA", "500", "600");
        var aircraft2 = new CargoAircraft("HRS", "700", "800");
        database.AddCargoAircraft(aircraft1);
        database.AddCargoAircraft(aircraft2);

        // Act
        var cargoAircrafts = database.GetCargoAircrafts();

        // Assert
        Assert.Contains(aircraft1.Model.ToLower(), cargoAircrafts.Keys);
        Assert.Contains(aircraft2.Model.ToLower(), cargoAircrafts.Keys);
    }

    [Fact]
    public void GetPassengerAircrafts_ReturnsAllPassengerAircrafts()
    {
        // Arrange
        var aircraft1 = new PassengerAircraft("Model1", "100", "200", "150");
        var aircraft2 = new PassengerAircraft("Model2", "200", "300", "200");
        database.AddPassengerAircraft(aircraft1);
        database.AddPassengerAircraft(aircraft2);

        // Act
        var passengerAircrafts = database.GetPassengerAircrafts();

        // Assert
        Assert.Contains(aircraft1.Model.ToLower(), passengerAircrafts.Keys);
        Assert.Contains(aircraft2.Model.ToLower(), passengerAircrafts.Keys);
    }

    [Fact]
    public void GetPrivateAircrafts_ReturnsAllPrivateAircrafts()
    {
        // Arrange
        var aircraft1 = new PrivateAircraft("Model1", "150");
        var aircraft2 = new PrivateAircraft("Model2", "200");
        database.AddPrivateAircraft(aircraft1);
        database.AddPrivateAircraft(aircraft2);

        // Act
        var privateAircrafts = database.GetPrivateAircrafts();

        // Assert
        Assert.Contains(aircraft1.Model.ToLower(), privateAircrafts.Keys);
        Assert.Contains(aircraft2.Model.ToLower(), privateAircrafts.Keys);
    }

    [Fact]
    public void GetAirportsByCriteria_ReturnsAirportsMatchingCriteria()
    {
        // Arrange
        var airport1 = new Airport("GVD", "Airport ABC", "City", "Country");
        var airport2 = new Airport("TRA", "Airport DEF", "City", "Country");
        database.AddAirport(airport1);
        database.AddAirport(airport2);

        // Act
        var airportsByCity = database.GetAirportsByCriteria("City", "City");
        var airportsByCountry = database.GetAirportsByCriteria("Country", "Country");

        // Assert
        Assert.Contains(airport1, airportsByCity);
        Assert.Contains(airport2, airportsByCountry);
    }

    [Fact]
    public void IsRouteExisting_ExistingRoute_ReturnsTrue()
    {
        // Arrange
        var route = new Route();
        database.AddRoute(route);

        // Act
        var isRouteExisting = database.IsRouteExisting();

        // Assert
        Assert.True(isRouteExisting);
    }


}
