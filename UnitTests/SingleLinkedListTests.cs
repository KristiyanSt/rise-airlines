﻿using System;
using System.Collections.Generic;
using Xunit;
using Airlines.Business.DataStructures;

namespace Airlines.Business.Tests
{
    public class SingleLinkedListTests
    {
        [Fact]
        public void AddFirst_EmptyList_ShouldAddItemAsHeadAndTail()
        {
            // Arrange
            var list = new SingleLinkedList<int>();

            // Act
            list.AddFirst(10);

            // Assert
            Assert.Equal(1u, list.Count);
            Assert.True(list.Contains(10));
        }

        [Fact]
        public void AddLast_EmptyList_ShouldAddItemAsHeadAndTail()
        {
            // Arrange
            var list = new SingleLinkedList<int>();

            // Act
            list.AddLast(10);

            // Assert
            Assert.Equal(1u, list.Count);
            Assert.True(list.Contains(10));
        }

        [Fact]
        public void RemoveFirst_SingleItemList_ShouldRemoveItem()
        {
            // Arrange
            var list = new SingleLinkedList<int>();
            list.AddFirst(10);

            // Act
            bool removed = list.RemoveFirst();

            // Assert
            Assert.True(removed);
            Assert.Equal(0u, list.Count);
            Assert.False(list.Contains(10));
        }

        [Fact]
        public void RemoveLast_SingleItemList_ShouldRemoveItem()
        {
            // Arrange
            var list = new SingleLinkedList<int>();
            list.AddFirst(10);

            // Act
            bool removed = list.RemoveLast();

            // Assert
            Assert.True(removed);
            Assert.Equal(0u, list.Count);
            Assert.False(list.Contains(10));
        }

        [Fact]
        public void Contains_ItemExistsInList_ShouldReturnTrue()
        {
            // Arrange
            var list = new SingleLinkedList<int>();
            list.AddFirst(10);
            list.AddLast(20);

            // Act & Assert
            Assert.True(list.Contains(10));
            Assert.True(list.Contains(20));
        }

        [Fact]
        public void Contains_ItemDoesNotExistInList_ShouldReturnFalse()
        {
            // Arrange
            var list = new SingleLinkedList<int>();
            list.AddFirst(10);
            list.AddLast(20);

            // Act & Assert
            Assert.False(list.Contains(30));
        }

        [Fact]
        public void ToStringList_ListWithItems_ShouldReturnCorrectList()
        {
            // Arrange
            var list = new SingleLinkedList<int>();
            list.AddFirst(10);
            list.AddLast(20);
            list.AddLast(30);

            // Act
            var stringList = list.ToStringList();

            // Assert
            Assert.Equal(new List<string> { "10", "20", "30" }, stringList);
        }

        [Fact]
        public void GetEnumerator_ShouldEnumerateItemsInCorrectOrder()
        {
            // Arrange
            var list = new SingleLinkedList<int>();
            list.AddLast(10);
            list.AddLast(20);
            list.AddLast(30);

            // Act
            var enumerator = list.GetEnumerator();
            var result = new List<int>();
            while (enumerator.MoveNext())
            {
                result.Add(enumerator.Current);
            }

            // Assert
            Assert.Equal(new List<int> { 10, 20, 30 }, result);
        }

        [Fact]
        public void GetLast_ShouldReturnLastItemInList()
        {
            // Arrange
            var list = new SingleLinkedList<int>();
            list.AddLast(10);
            list.AddLast(20);
            list.AddLast(30);

            // Act
            var last = list.GetLast();

            // Assert
            Assert.NotNull(last);
            Assert.Equal(30, last.Value);
        }

        [Fact]
        public void GetLast_EmptyList_ShouldReturnNull()
        {
            // Arrange
            var list = new SingleLinkedList<int>();

            // Act
            var last = list.GetLast();

            // Assert
            Assert.Null(last);
        }
    }
}
