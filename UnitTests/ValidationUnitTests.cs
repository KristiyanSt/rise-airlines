using System;
using Airlines.Business.Exceptions;
using Airlines.Business.Facilities;
using Airlines.Business.Utils;
using Xunit;

namespace Airlines.Tests
{
    public class ValidationTests
    {
        [Theory]
        [InlineData("", "")]
        [InlineData("   ", "")]
        [InlineData("ABC", "ABC")]
        public void ValidateString_ShouldReturnValidString(string input, string expected)
        {
            // Act
            var result = Validation.ValidateString(input);

            // Assert
            Assert.Equal(expected, result);
        }

        [Theory]
        [InlineData(new string[] { "JFK", "LAX" }, "JFK")]
        public void ValidateAirport_ShouldNotThrowExceptionForValidAirport(string[] airportData, string airport)
        {
            // Act & Assert
            Assert.ThrowsAny<Exception>(() => Validation.ValidateAirport(airportData, airport));
        }

        [Theory]
        [InlineData(new string[] { "JFK", "LAX" }, "JFK")]
        [InlineData(new string[] { "JFK", "LAX" }, "LAX")]
        public void ValidateAirport_ShouldThrowExceptionForDuplicateAirport(string[] airportData, string airport)
        {
            // Act & Assert
            Assert.Throws<RecordAlreadyExistsException>(() => Validation.ValidateAirport(airportData, airport));
        }

        [Theory]
        [InlineData(new string[] { "Delta", "American" }, "Delta")]
        public void ValidateAirline_ShouldNotThrowExceptionForValidAirline(string[] airlineData, string airline)
        {
            // Act & Assert
            Assert.ThrowsAny<Exception>(() => Validation.ValidateAirline(airlineData, airline));
        }


        [Theory]
        [InlineData(new string[] { "ABC123", "XYZ456" }, "ABC123")]
        public void ValidateFlight_ShouldNotThrowExceptionForValidFlight(string[] flightData, string flight)
        {
            // Act & Assert
            Assert.ThrowsAny<Exception>(() => Validation.ValidateFlight(flightData, flight));
        }

        [Theory]
        [InlineData(new string[] { "ABC123", "XYZ456" }, "ABC123")]
        [InlineData(new string[] { "ABC123", "XYZ456" }, "XYZ456")]
        public void ValidateFlight_ShouldThrowExceptionForDuplicateFlight(string[] flightData, string flight)
        {
            // Act & Assert
            Assert.Throws<RecordAlreadyExistsException>(() => Validation.ValidateFlight(flightData, flight));
        }
    }
}
