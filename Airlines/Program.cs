﻿using Airlines.Business;
using Airlines.Business.DataStructures;
using Airlines.Business.Facilities;
using Airlines.Business.Strategies;
using Airlines.Business.Utils;


namespace Airlines.Console
{
    public class Program
    {
        private static void Main()
        {
            CommandHandler commandHandler = new CommandHandler();
            ConsoleDataHandler consoleDataHandler = new ConsoleDataHandler(commandHandler);
            FileDataHandler fileDataHandler = new FileDataHandler();

            fileDataHandler.CollectFilesData();
            consoleDataHandler.CollectConsoleData();

            //initialize strategy along with flight network
            ShortestRouteStrategy defaultStrategy = new ShortestRouteStrategy();
            FlightNetwork flightNetwork = new FlightNetwork(defaultStrategy);

            //populate airport and flight data into flight network
            flightNetwork.PopulateNetwork();

            //"save" the flight network in database with the help of flight network service
            Business.Services.FlightNetworkService.AddFlightNetwork(flightNetwork);

            List<Airport> sortedAirports = Business.Services.AirportService.GetAirports().DefaultSort();
            List<Airline> sortedAirlines = Business.Services.AirlineService.GetAirlines().DefaultSort();
            List<Flight> sortedFlights = Business.Services.FlightService.GetFlights().DefaultSort();

            PrintHandler.DisplayListData(sortedAirports, "Airports");
            PrintHandler.DisplayListData(sortedAirlines, "Airlines");
            PrintHandler.DisplayListData(sortedFlights, "Flights");
            PrintHandler.DisplayCommandInfo();

            consoleDataHandler.ContinuouslyReadAndDelegateCommandInput();
        }
    }
}

