﻿using Airlines.Business.Exceptions;
using Airlines.Business.Facilities;

namespace Airlines.Business.Utils;
public static class Searcher
{
    public static string IdentifyMatch(Airport[] airports, Airline[] airlines, Flight[] flights, string searchTerm)
    {
        var airportSearchResult = BinarySearch(airports, searchTerm);
        if (airportSearchResult >= 0)
        {
            return "Airport";
        }

        var airlineSearchResult = BinarySearch(airlines, searchTerm);
        if (airlineSearchResult >= 0)
        {
            return "Airline";
        }

        var flightSearchResult = BinarySearch(flights, searchTerm);
        if (flightSearchResult >= 0)
        {
            return "Flight";
        }

        throw new NotFoundException("Searched item was not found");
    }
    public static int LinearSearch<T>(T[] arr, string searchTerm)
    {
        for (var i = 0; i < arr.Length; i++)
        {
            if (arr[i] != null)
            {
                var currentItem = arr[i].ToString();
                if (currentItem == searchTerm)
                    return i;
            }
        }
        return -1;
    }
    public static int BinarySearch<T>(T[] arr, string searchTerm)
    {
        var leftPosition = 0;
        var rightPosition = arr.Length - 1;

        while (leftPosition <= rightPosition)
        {
            var mid = leftPosition + (rightPosition - leftPosition) / 2;

            // Check if searchName is present at mid
            var middleItem = arr[mid].ToString();
            if (middleItem == searchTerm)
                return mid;

            // If search name's first letter is greater, ignore left half
            var searchNameFirstLetter = searchTerm[0];
            var middleItemFirstLetter = middleItem[0];

            if (searchNameFirstLetter.CompareTo(middleItemFirstLetter) > 0)
                leftPosition = mid + 1;
            // If search name's first letter is smaller, ignore right half
            else
                rightPosition = mid - 1;
        }

        // If we reach here, then item was not present
        return -1;
    }
}
