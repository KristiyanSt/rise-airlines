﻿namespace Airlines.Business.Utils;
public static class Sorter
{
    public static void SelectionSort<T>(this T[] arr, string orderBy) where T : class
    {
        for (int i = 0; i < arr.Length - 1; i++)
        {
            int selectedStringIndex = i;
            for (int j = i + 1; j < arr.Length; j++)
            {
                int currentStringIndex = j;

                bool comparisonChecking = orderBy == "descending"
                    ? arr[selectedStringIndex].ToString().CompareTo(arr[currentStringIndex].ToString()) < 0
                    : arr[selectedStringIndex].ToString().CompareTo(arr[currentStringIndex].ToString()) > 0;

                if (comparisonChecking)
                {
                    T temp = arr[selectedStringIndex];
                    arr[selectedStringIndex] = arr[currentStringIndex];
                    arr[currentStringIndex] = temp;
                }
            }
        }
    }
    public static void BubbleSort<T>(this T[] arr, string orderBy) where T : class
    {
        int arrLength = arr.Length;
        for (int i = 0; i < arrLength - 1; i++)
        {
            for (int j = 0; j < arrLength - i - 1; j++)
            {
                T currentItem = arr[j];
                T nextItem = arr[j + 1];

                bool comparisonChecking = orderBy == "descending"
                   ? currentItem.ToString().CompareTo(nextItem.ToString()) < 0
                   : currentItem.ToString().CompareTo(nextItem.ToString()) > 0;

                if (comparisonChecking)
                {
                    arr[j] = nextItem;
                    arr[j + 1] = currentItem;
                }
            }
        }
    }

    public static List<T> DefaultSort<T>(this List<T> list) where T : class
    {
        return list.OrderBy(x => x.ToString()).ToList();
    }

    public static List<T> DescendingSort<T>(this List<T> list) where T : class
    {
        return list.OrderByDescending(x => x.ToString()).ToList();
    }
}
