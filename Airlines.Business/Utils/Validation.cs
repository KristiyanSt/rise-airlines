﻿using Airlines.Business.Exceptions;
using Airlines.Business.Facilities;

namespace Airlines.Business.Utils;
public static class Validation
{
    public static string ValidateString(string? str)
    {
        str = str.Trim();
        return str is not null ? str : string.Empty;
    }

    public static void ValidateAirport<T>(T[] airportData, string airport)
    {
        airport = airport.Trim();
        if (airport.Length != 3)
        {
            throw new InvalidInputException("Invalid airport name, it must consist of exactly 3 characters");
        }
        foreach (char c in airport)
        {
            if (!char.IsLetter(c))
            {
                throw new InvalidInputException("Invalid airport name, must consist only of only alphabetic characters");
            }
        }
        UniquenessCheck(airportData, airport);
    }

    public static void ValidateAirline<T>(T[] airlineData, string airline)
    {
        airline = airline.Trim();
        if (airline.Length == 0 || airline.Length >= 6)
        {
            throw new InvalidInputException("Invalid airline name, it must have a length between 1 and 5 characters");
        }
        UniquenessCheck(airlineData, airline);
    }

    public static void ValidateFlight<T>(T[] flightData, string flight)
    {
        flight = flight.Trim();
        if (!flight.All(char.IsLetterOrDigit) || flight.Length == 0)
        {
            throw new InvalidInputException("Invalid flight identifier, it must contain only alphanumeric symbols");
        }
        UniquenessCheck(flightData, flight);
    }

    public static void UniquenessCheck<T>(T[] arr, string searchTerm)
    {
        int searchResult = Searcher.LinearSearch(arr, searchTerm);
        if (searchResult >= 0)
        {
            throw new RecordAlreadyExistsException($"{searchTerm} already exists");
        }
    }
}