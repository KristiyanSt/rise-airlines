﻿using Airlines.Business.Facilities;
using Airlines.Business.Exceptions;
using Airlines.Business.DataStructures;

namespace Airlines.Business.DatabaseNS;
public sealed class Database
{
    private static Database _instance;
    private Dictionary<string, Airport> _airports = [];
    private Dictionary<string, Airline> _airlines = [];
    private Dictionary<string, Flight> _flights = [];
    private Dictionary<string, CargoAircraft> _cargoAircrafts = [];
    private Dictionary<string, PassengerAircraft> _passengerAircrafts = [];
    private Dictionary<string, PrivateAircraft> _privateAircrafts = [];
    private Dictionary<string, List<Airport>> _airportsByCity = [];
    private Dictionary<string, List<Airport>> _airportsByCountry = [];
    private Route? _route;
    public Tree? _flightRoutesTree;
    private FlightNetwork? _flightNetwork;
    //all aircrafts ?
    private Database() { }
    public static Database GetInstance()
    {
        if (_instance == null)
        {
            _instance = new Database();
        }
        return _instance;
    }

    public static Database Reset()
    {
        _instance = new Database();
        return _instance;
    }

    public FlightNetwork AddFlightNetwork(FlightNetwork flightNetwork)
    {
        this._flightNetwork = flightNetwork;
        return this._flightNetwork;
    }
    public FlightNetwork GetFlightNetwork()
    {
        return this._flightNetwork!;
    }
    public Airport AddAirport(Airport airport)
    {
        //add in all airports
        if (this._airports.ContainsKey(airport.Identifier.ToLower()))
        {
            throw new RecordAlreadyExistsException($"Airport {airport.Identifier} already exists");
        }
        this._airports.Add(airport.Identifier.ToLower(), airport);

        //add in dict of airports by city
        if (this._airportsByCity.ContainsKey(airport.City.ToLower()))
        {
            List<Airport> airportsByCity = this._airportsByCity[airport.City.ToLower()];
            if (airportsByCity.Any(x => x.Identifier.ToLower() == airport.Identifier.ToLower()))
            {
                throw new RecordAlreadyExistsException($"Airport {airport.Identifier} already exists in {airport.City}");
            }
            airportsByCity.Add(airport);
        }
        else
        {
            this._airportsByCity.Add(airport.City.ToLower(), [airport]);
        }

        //add in dict of airports by country
        if (this._airportsByCountry.ContainsKey(airport.Country.ToLower()))
        {
            List<Airport> airportsByCountry = this._airportsByCountry[airport.Country.ToLower()];
            if (airportsByCountry.Any(x => x.Identifier.ToLower() == airport.Identifier.ToLower()))
            {
                throw new RecordAlreadyExistsException($"Airport {airport.Identifier} already exists in {airport.Country}");
            }
            airportsByCountry.Add(airport);
        }
        else
        {
            this._airportsByCountry.Add(airport.Country.ToLower(), [airport]);
        }

        return airport;
    }
    public Tree AddFlightRoutesTree(Tree flightRoutesTree)
    {
        this._flightRoutesTree = flightRoutesTree;
        return this._flightRoutesTree;
    }
    public Tree GetFlightRoutesTree()
    {
        if (this._flightRoutesTree == null)
        {
            throw new NotFoundException("No flight routes persist in program");
        }
        return this._flightRoutesTree;
    }

    public Airline AddAirline(Airline airline)
    {
        string airlineName = airline.Name.ToLower();
        if (this._airlines.ContainsKey(airlineName))
        {
            throw new RecordAlreadyExistsException($"Airline {airline.Name} already exists");
        }
        _airlines.Add(airlineName, airline);
        return airline;
    }

    public Flight AddFlight(Flight flight)
    {
        //check for valid departure and arrival airports
        if (!this._airports.ContainsKey(flight.DepartureAirport.ToLower()))
        {
            throw new InvalidInputException($"Invalid departure airport in flight {flight.Identifier}: {flight.ArrivalAirport}");
        }
        if (!this._airports.ContainsKey(flight.ArrivalAirport.ToLower()))
        {
            throw new InvalidInputException($"Invalid arrival airport in flight {flight.Identifier}: {flight.ArrivalAirport}");
        }

        string flightId = flight.Identifier.ToLower();
        if (this._flights.ContainsKey(flightId))
        {
            throw new RecordAlreadyExistsException($"Flight {flight.Identifier} already exists");
        }
        this._flights.Add(flightId, flight);
        return flight;
    }

    public CargoAircraft AddCargoAircraft(CargoAircraft aircraft)
    {
        if (this._cargoAircrafts.ContainsKey(aircraft.Model.ToLower()))
        {
            throw new RecordAlreadyExistsException($"Aircraft ${aircraft.Model} already persists");
        }
        this._cargoAircrafts.Add(aircraft.Model.ToLower(), aircraft);
        return aircraft;
    }

    public PassengerAircraft AddPassengerAircraft(PassengerAircraft aircraft)
    {
        if (this._passengerAircrafts.ContainsKey(aircraft.Model.ToLower()))
        {
            throw new RecordAlreadyExistsException($"Aircraft ${aircraft.Model} already persists");
        }
        this._passengerAircrafts.Add(aircraft.Model.ToLower(), aircraft);
        return aircraft;
    }

    public PrivateAircraft AddPrivateAircraft(PrivateAircraft aircraft)
    {
        if (this._privateAircrafts.ContainsKey(aircraft.Model.ToLower()))
        {
            throw new RecordAlreadyExistsException($"Aircraft ${aircraft.Model} already persists");
        }
        this._privateAircrafts.Add(aircraft.Model.ToLower(), aircraft);
        return aircraft;
    }

    public bool IsAirlineExisting(string query)
    {
        query = query.ToLower();
        return this._airlines.ContainsKey(query);
    }

    public Flight? FindById(string flightId)
    {
        flightId = flightId.ToLower();
        if (this._flights.ContainsKey(flightId))
        {
            return this._flights[flightId];
        }
        return null;
    }
    public Aircraft? FindAircraftByModel(string model)
    {
        model = model.ToLower();
        if (this._cargoAircrafts.ContainsKey(model))
        {
            return this._cargoAircrafts[model];
        }
        if (this._privateAircrafts.ContainsKey(model))
        {
            return this._privateAircrafts[model];
        }
        if (this._passengerAircrafts.ContainsKey(model))
        {
            return this._passengerAircrafts[model];
        }
        return null;
    }

    public string PerformSearch(string query)
    {
        query = query.ToLower();
        if (this._airports.Keys.Any(x => x.ToString().ToLower() == query))
        {
            return "Airport";
        }

        if (this._airlines.Keys.Any(x => x.ToString().ToLower() == query))
        {
            return "Airline";
        }

        if (this._flights.Keys.Any(x => x.ToString().ToLower() == query))
        {
            return "Flight";
        }

        throw new NotFoundException($"Searched item {query} was not found");
    }

    public Dictionary<string, Airport> GetAirports()
    {
        return this._airports;
    }
    public Airport GetAirportById(string airportId)
    {
        airportId = airportId.ToLower();
        if (!this._airports.ContainsKey(airportId))
        {
            throw new InvalidInputException($"No airport correspond to identifier {airportId}");
        }
        return this._airports[airportId];
    }
    public Flight GetFlightById(string flightId)
    {
        flightId = flightId.ToLower();
        if (!this._flights.ContainsKey(flightId))
        {
            throw new InvalidInputException($"No flight correspond to identifier {flightId}");
        }
        return this._flights[flightId];
    }

    public Dictionary<string, Airline> GetAirlines()
    {
        return this._airlines;
    }

    public Dictionary<string, Flight> GetFlights()
    {
        return this._flights;
    }

    public List<Flight> GetFlightsByDepartureAirportId(string departureAirportId)
    {
        return this._flights.Values.Where(fl => fl.DepartureAirport == departureAirportId).ToList();
    }

    public Dictionary<string, CargoAircraft> GetCargoAircrafts()
    {
        return this._cargoAircrafts;
    }

    public Dictionary<string, PassengerAircraft> GetPassengerAircrafts()
    {
        return this._passengerAircrafts;
    }

    public Dictionary<string, PrivateAircraft> GetPrivateAircrafts()
    {
        return this._privateAircrafts;
    }

    public List<Airport> GetAirportsByCriteria(string inputData, string criteria)
    {
        inputData = inputData.ToLower();
        criteria = criteria.ToLower();

        if (criteria == "city")
        {
            if (this._airportsByCity.ContainsKey(inputData))
            {
                return this._airportsByCity[inputData];
            }
        }
        else if (criteria == "country")
        {
            if (this._airportsByCountry.ContainsKey(inputData))
            {
                return this._airportsByCountry[inputData];
            }
        }
        throw new InvalidInputException($"No airports correspond to given {criteria}");
    }

    public void AddRoute(Route route)
    {
        if (this._route != null)
        {
            throw new RecordAlreadyExistsException("Route has been initialized in the program");
        }
        this._route = route;
    }
    public bool IsRouteExisting()
    {
        return this._route != null;
    }
    public Route GetRoute()
    {
        if (this._route != null)
        {
            return this._route;
        }
        throw new NotFoundException("No route has been initialized");
    }
}
