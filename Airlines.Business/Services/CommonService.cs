﻿using Airlines.Business.DatabaseNS;
using Airlines.Business.Exceptions;
using Airlines.Business.Facilities;
using System.Linq;

namespace Airlines.Business.Services;
public static class CommonService
{
    public static string PerformSearch(string query)
    {
        Database database = Database.GetInstance();
        string matchFacility = database.PerformSearch(query);
        return matchFacility.StartsWith("A") ? $"an {matchFacility}" : $"a {matchFacility}";
    }

    public static (List<string> arr, string) SortDataByOrder(string facilityType, string orderBy)
    {
        Database database = Database.GetInstance();
        switch (facilityType)
        {
            case "airports":
                Dictionary<string, Airport> airports = database.GetAirports();
                List<string> orderedAirports = orderBy == "ascending"
                    ? airports.OrderBy(x => x.Value.Identifier).Select(x => x.Value.ToString()).ToList()
                    : airports.OrderByDescending(x => x.Value.Identifier).Select(x => x.Value.ToString()).ToList();
                return (orderedAirports, "Airports");
            case "airlines":
                Dictionary<string, Airline> airlines = database.GetAirlines();
                List<string> orderedAirlines = orderBy == "ascending"
                     ? airlines.OrderBy(x => x.Value.Name).Select(x => x.Value.ToString()).ToList()
                     : airlines.OrderByDescending(x => x.Value.Name).Select(x => x.Value.ToString()).ToList();
                return (orderedAirlines, "Airlines");
            case "flights":
                Dictionary<string, Flight> flights = database.GetFlights();
                List<string> orderedFlights = orderBy == "ascending"
                      ? flights.OrderBy(x => x.Value.Identifier).Select(x => x.Value.ToString()).ToList()
                      : flights.OrderByDescending(x => x.Value.Identifier).Select(x => x.Value.ToString()).ToList();
                return (orderedFlights, "Flights");
        }
        throw new InvalidInputException("Invalid input data");
    }
}
