﻿using Airlines.Business.DatabaseNS;
using Airlines.Business.Exceptions;
using Airlines.Business.Facilities;

namespace Airlines.Business.Services;
public static class FlightService
{
    public static List<Flight> GetFlights()
    {
        Database database = Database.GetInstance();
        return database.GetFlights().Values.ToList();
    }
    public static Flight AddFlight(string flightId, string departureAirport, string arrivalAirport, string aircraftModel, string price, string time)
    {
        Database database = Database.GetInstance();
        Aircraft? queriedAircraft = database.FindAircraftByModel(aircraftModel);
        if (queriedAircraft == null)
        {
            throw new InvalidInputException($"Invalid aircraft model in flight {flightId}");
        }
        return database.AddFlight(new Flight(flightId, departureAirport, arrivalAirport, queriedAircraft, price, time));
    }
    public static Flight GetFlightById(string flightId)
    {
        Database database = Database.GetInstance();
        return database.GetFlightById(flightId);
    }
    public static List<Flight> GetFlightsByDepartureAirportId(string departureAirportId)
    {
        Database database = Database.GetInstance();
        return database.GetFlightsByDepartureAirportId(departureAirportId);
    }
}
