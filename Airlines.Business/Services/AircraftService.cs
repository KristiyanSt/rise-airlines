﻿using Airlines.Business.DatabaseNS;
using Airlines.Business.Exceptions;
using Airlines.Business.Facilities;

namespace Airlines.Business.Services;
public static class AircraftService
{
    public static string ReserveCargo(string flightId, string cargoWeight, string cargoVolume)
    {
        Database database = Database.GetInstance();
        Flight? queriedFlight = database.FindById(flightId);
        if (queriedFlight == null)
        {
            throw new InvalidInputException("Invalid flight identifier");
        }
        if (queriedFlight.Aircraft.GetType() != typeof(CargoAircraft))
        {
            throw new ArgumentException($"{queriedFlight.Identifier}'s aircraft is not a cargo aircraft");
        }
        CargoAircraft cargoAircraftParsed = (CargoAircraft)queriedFlight.Aircraft;

        double cargoWeightParsed;
        bool isCargoWeightParsed = double.TryParse(cargoWeight, out cargoWeightParsed);
        if (!isCargoWeightParsed || (isCargoWeightParsed && cargoWeightParsed <= 0))
        {
            throw new InvalidInputException("Invalid cargo weight");
        }
        double cargoVolumeParsed;
        bool isCargoVolumeParsed = double.TryParse(cargoVolume, out cargoVolumeParsed);
        if (!isCargoVolumeParsed || (isCargoVolumeParsed && cargoVolumeParsed <= 0))
        {
            throw new InvalidInputException("Invalid cargo volume");
        }

        bool isReserved = cargoAircraftParsed.ReserveCargo(cargoWeightParsed, cargoVolumeParsed);
        return isReserved ? "successful reservation" : "reservation failed";
    }

    public static string ReserveTicket(string flightId, string seats, string smallBaggageCount, string largeBaggageCount)
    {
        Database database = Database.GetInstance();
        Flight? queriedFlight = database.FindById(flightId);
        if (queriedFlight == null)
        {
            throw new InvalidInputException($"Invalid flight identifier {flightId}");
        }
        if (queriedFlight.Aircraft.GetType() != typeof(PassengerAircraft))
        {
            throw new Exception($"{queriedFlight.Identifier}'s aircraft is not a passenger aircraft");
        }
        PassengerAircraft queriedAircraft = (PassengerAircraft)queriedFlight.Aircraft;

        //could possibly reserve a ticket with 0 baggages count
        int seatsParsed;
        bool isSeatsParsed = int.TryParse(seats, out seatsParsed);
        if (!isSeatsParsed || (isSeatsParsed && seatsParsed <= 0))
        {
            throw new InvalidInputException("Invalid seats count");
        }
        int smallBaggageCountParsed;
        bool isSmallBaggageCountParsed = int.TryParse(smallBaggageCount, out smallBaggageCountParsed);
        if (!isSmallBaggageCountParsed)
        {
            throw new InvalidInputException("Invalid small baggage count");
        }
        int largeBaggageCountParsed;
        bool isLargeBaggageCountParsed = int.TryParse(largeBaggageCount, out largeBaggageCountParsed);
        if (!isLargeBaggageCountParsed)
        {
            throw new InvalidInputException("Invalid large baggage count");
        }

        bool isReserved = queriedAircraft.ReserveTicket(seatsParsed, smallBaggageCountParsed, largeBaggageCountParsed);
        return isReserved ? "successful reservation" : "reservation failed";
    }

    public static PrivateAircraft AddPrivateAircraft(string model, string seats)
    {
        Database database = Database.GetInstance();
        return database.AddPrivateAircraft(new PrivateAircraft(model, seats));
    }

    public static CargoAircraft AddCargoAircraft(string model, string cargoCapacity, string cargoVolume)
    {
        Database database = Database.GetInstance();
        return database.AddCargoAircraft(new CargoAircraft(model, cargoCapacity, cargoVolume));
    }

    public static PassengerAircraft AddPassengerAircraft(string model, string cargoCapacity, string cargoVolume, string seats)
    {
        Database database = Database.GetInstance();
        return database.AddPassengerAircraft(new PassengerAircraft(model, cargoCapacity, cargoVolume, seats));
    }
}
