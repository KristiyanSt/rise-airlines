﻿using Airlines.Business.DatabaseNS;
using Airlines.Business.Facilities;

namespace Airlines.Business.Services;
public static class AirportService
{
    public static List<Airport> GetAirports()
    {
        Database database = Database.GetInstance();
        return database.GetAirports().Values.ToList();
    }
    public static List<Airport> GetAirportsByCriteria(string facilityType, string criteria)
    {
        Database database = Database.GetInstance();
        return database.GetAirportsByCriteria(facilityType, criteria);
    }
    public static Airport AddAirport(string identifier, string name, string city, string country)
    {
        Database database = Database.GetInstance();
        return database.AddAirport(new Airport(identifier, name, city, country));
    }

    public static Airport GetAirportById(string airportId)
    {
        Database database = Database.GetInstance();
        return database.GetAirportById(airportId);
    }
}
