﻿using Airlines.Business.DatabaseNS;
using Airlines.Business.DataStructures;
using System.ComponentModel.DataAnnotations.Schema;

namespace Airlines.Business.Services;
public static class FlightNetworkService
{
    public static FlightNetwork AddFlightNetwork(FlightNetwork flightNetwork)
    {
        Database database = Database.GetInstance();
        return database.AddFlightNetwork(flightNetwork);
    }
    public static FlightNetwork GetFlightNetwork()
    {
        Database database = Database.GetInstance();
        return database.GetFlightNetwork();
    }
}
