﻿using Airlines.Business.DatabaseNS;
using Airlines.Business.Exceptions;
using Airlines.Business.Facilities;

namespace Airlines.Business.Services;
public static class RouteService
{
    public static void AddToRoute(string flightId)
    {
        Database database = Database.GetInstance();
        if (database.IsRouteExisting())
        {
            Route route = database.GetRoute();
            Flight? queriedFlight = database.FindById(flightId);
            if (queriedFlight == null)
            {
                throw new InvalidInputException("Invalid flight identifier");
            }
            route.AppendFlight(queriedFlight);
        }
        else
        {
            throw new NotFoundException("Route has not been initialized in the program");
        }
    }
    public static void AddRoute()
    {
        Database database = Database.GetInstance();
        database.AddRoute(new Route());
    }
    public static void RemoveLastInRoute()
    {
        Database database = Database.GetInstance();
        if (database.IsRouteExisting())
        {
            Route route = database.GetRoute();
            route.RemoveLastFlight();
        }
        else
        {
            throw new NotFoundException("Route has not been initialized in the program");
        }
    }
    public static List<string> GetRoute()
    {
        Database database = Database.GetInstance();
        if (database.IsRouteExisting())
        {
            Route route = database.GetRoute();
            return route.ToStringList();
        }
        throw new NotFoundException("Route has not been initialized in the program");
    }
}
