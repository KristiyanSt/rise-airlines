﻿using Airlines.Business.DatabaseNS;
using Airlines.Business.Facilities;

namespace Airlines.Business.Services;
public static class AirlineService
{
    public static List<Airline> GetAirlines()
    {
        Database database = Database.GetInstance();
        return database.GetAirlines().Values.ToList();
    }
    public static string IsAirlineExisting(string query)
    {
        Database database = Database.GetInstance();
        bool airlineExists = database.IsAirlineExisting(query);
        if (airlineExists)
        {
            return "true";
        }

        return "false";
    }

    public static Airline AddAirline(string name)
    {
        Database database = Database.GetInstance();
        return database.AddAirline(new Airline(name));
    }
}
