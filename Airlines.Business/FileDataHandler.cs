﻿using Airlines.Business.DatabaseNS;
using Airlines.Business.DataStructures;
using Airlines.Business.Exceptions;
using Airlines.Business.Facilities;
using Airlines.Business.Services;

namespace Airlines.Business;
public class FileDataHandler
{
    public void CollectFilesData()
    {
        this.ReadAndCollectFileData("../../../StaticData/AirportData.csv", "airport");
        this.ReadAndCollectFileData("../../../StaticData/AirlineData.csv", "airline");
        this.ReadAndCollectFileData("../../../StaticData/AircraftData.csv", "aircraft");
        this.ReadAndCollectFileData("../../../StaticData/FlightData.csv", "flight");
        this.ReadAndCollectFileData("../../../StaticData/Route.csv", "route");
    }
    public void ReadAndCollectFileData(string filePath, string facilityType)
    {
        //TODO create custom reading for each file data type
        try
        {
            if (!File.Exists(filePath))
            {
                throw new ArgumentException("The program encountered an error");
            }

            // Read from the CSV file using StreamReader, make use of "using" 
            using (StreamReader reader = new StreamReader(filePath))
            {
                while (!reader.EndOfStream)
                {
                    string? line = reader.ReadLine();
                    if (string.IsNullOrEmpty(line))
                    {
                        continue;
                    }

                    //read flight routes data from file Route.csv
                    if (facilityType == "route")
                    {
                        string readAirport = line;
                        Airport startingAirport = AirportService.GetAirportById(readAirport);
                        Database db = Database.GetInstance();

                        //create flight routes tree from a starting airport
                        Tree flightRoutesTree = db.AddFlightRoutesTree(new Tree(new TreeNode(startingAirport)));

                        while (!reader.EndOfStream)
                        {
                            string? flightId = reader.ReadLine();
                            if (string.IsNullOrEmpty(flightId))
                            {
                                continue;
                            }
                            Flight flight = FlightService.GetFlightById(flightId);

                            //try to connect current flight to the flight routes tree 
                            flightRoutesTree.TryToConnect(flightRoutesTree.Root, flight);
                        }
                        return;
                    }

                    string[] fields;

                    switch (facilityType)
                    {
                        case "airport":
                            fields = line.Split(", ", StringSplitOptions.RemoveEmptyEntries);
                            if (fields.Length != 4)
                            {
                                throw new InvalidInputException("Invalid airport input");
                            }
                            string identifier = fields[0];
                            string name = fields[1];
                            string city = fields[2];
                            string country = fields[3];
                            AirportService.AddAirport(identifier, name, city, country);
                            break;
                        case "airline":
                            fields = line.Split(", ", StringSplitOptions.RemoveEmptyEntries);
                            if (fields.Length != 1)
                            {
                                throw new InvalidInputException("Invalid airline input");
                            }
                            string airlineName = fields[0];
                            AirlineService.AddAirline(airlineName);
                            break;
                        case "flight":
                            fields = line.Split(",", StringSplitOptions.RemoveEmptyEntries);
                            if (fields.Length != 6)
                            {
                                throw new InvalidInputException("Invalid flight input");
                            }
                            string flightId = fields[0];
                            string departureAirport = fields[1];
                            string arrivalAirport = fields[2];
                            string aircraftModel = fields[3];
                            string price = fields[4];
                            string time = fields[5];
                            FlightService.AddFlight(flightId, departureAirport, arrivalAirport, aircraftModel, price, time);
                            break;
                        case "aircraft":
                            fields = line.Split(", ", StringSplitOptions.RemoveEmptyEntries);
                            if (fields.Length != 4)
                            {
                                throw new InvalidInputException("Invalid aircraft input");
                            }
                            string model = fields[0];
                            string cargoCapacity = fields[1];
                            string cargoVolume = fields[2];
                            string seats = fields[3];
                            if (cargoCapacity == "-")
                            {
                                AircraftService.AddPrivateAircraft(model, seats);
                            }
                            else if (seats == "-")
                            {
                                AircraftService.AddCargoAircraft(model, cargoCapacity, cargoVolume);
                            }
                            else
                            {
                                AircraftService.AddPassengerAircraft(model, cargoCapacity, cargoVolume, seats);
                            }
                            break;
                    }
                }
            }
        }
        catch (Exception e)
        {
            PrintHandler.DisplayMessage(e.Message);
        }
    }
}
