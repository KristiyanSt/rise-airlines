﻿using Airlines.Business.DTOs;
using Airlines.Business.Interfaces;
using Airlines.Persistence.Basic.Entities;
using Airlines.Persistence.Basic.Interfaces;
using System.Linq.Expressions;
using Airlines.Business.Exceptions;

namespace Airlines.Business.WebServices;
public class FlightService : IFlightService
{
    private IFlightRepository _flightRepository;
    public FlightService(IFlightRepository flightRepository)
    {
        this._flightRepository = flightRepository;
    }

    public IQueryable<Flight> GetAll()
    {
        return this._flightRepository.GetAll();
    }
    public IQueryable<Flight> Find(Expression<Func<Flight, bool>> predicate)
    {
        return this._flightRepository.Find(predicate);
    }
    public async Task<Flight?> GetFlightById(int id)
    {
        return await this._flightRepository.GetById(id);
    }
    public async Task<Flight?> GetFlightWithAirports(int id)
    {
        return await this._flightRepository.GetFlightWithAirports(id);
    }
    public IQueryable<Flight> FindDepartingFlightsByAirport(string airport)
    {
        return this._flightRepository
            .Find(f => f.DepartureAirport == airport && f.DepartureDateTime > DateTime.Now);
    }

    public async Task AddFlight(FlightDTO flightDTO)
    {
        Flight flightToCreate = new Flight
        {
            ArrivalAirport = flightDTO.ArrivalAirport,
            ArrivalDateTime = flightDTO.ArrivalDateTime,
            DepartureAirport = flightDTO.DepartureAirport,
            DepartureDateTime = flightDTO.DepartureDateTime,
            FlightNumber = flightDTO.FlightNumber,
        };

        await this._flightRepository.Add(flightToCreate);
        await this._flightRepository.Complete();
    }
    public async Task UpdateFlight(FlightDTO updatedFlightDTO, int id)
    {
        //this._flightRepository.Update(this._autoMapper.Map<Flight>(updatedFlight));
        Flight? flight = await this._flightRepository.GetById(id);
        if (flight == null)
        {
            throw new NotFoundException($"Flight with id {id} does not persist");
        }

        flight.FlightNumber = updatedFlightDTO.FlightNumber;
        flight.DepartureAirport = updatedFlightDTO.DepartureAirport;
        flight.ArrivalAirport = updatedFlightDTO.ArrivalAirport;
        flight.DepartureDateTime = updatedFlightDTO.DepartureDateTime;
        flight.ArrivalDateTime = updatedFlightDTO.ArrivalDateTime;

        await this._flightRepository.Complete();
    }
    public async Task DeleteFlight(int id)
    {
        Flight? flightToRemove = await this._flightRepository.GetById(id);
        if (flightToRemove == null)
        {
            throw new NotFoundException($"Flight with id {id} does not persist");
        }

        this._flightRepository.Remove(flightToRemove);
        await this._flightRepository.Complete();
    }
}
