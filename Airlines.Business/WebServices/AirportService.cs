﻿using Airlines.Business.Interfaces;
using Airlines.Business.DTOs;
using Airlines.Persistence.Basic.Entities;
using Airlines.Persistence.Basic.Interfaces;
using System.Linq.Expressions;
using Airlines.Business.Exceptions;

namespace Airlines.Business.WebServices;
public class AirportService : IAirportService
{
    private IAirportRepository _airportRepository;
    public AirportService(IAirportRepository airportRepository)
    {
        this._airportRepository = airportRepository;
    }

    public IQueryable<Airport> GetAll()
    {
        return this._airportRepository.GetAll();
    }

    public IQueryable<Airport> Find(Expression<Func<Airport, bool>> predicate)
    {
        return this._airportRepository.Find(predicate);
    }
    public IQueryable<Airport> GetAirportsByCity(string city)
    {
        return this._airportRepository.Find(a => a.City == city);
    }
    public async Task<Airport?> GetAirportByCode(string code)
    {
        return await this._airportRepository.SingleOrDefault(a => a.Code == code);
    }
    public async Task AddAirport(AirportDTO airportDTO)
    {
        Airport? existingAirport = await this._airportRepository.SingleOrDefault(a => a.Code == airportDTO.Code);
        if (existingAirport != null)
        {
            throw new RecordAlreadyExistsException($"Airport with {airportDTO.Code} code already exists");
        }

        Airport airportToCreate = new Airport
        {
            Code = airportDTO.Code,
            Name = airportDTO.Name,
            Founded = airportDTO.Founded,
            Country = airportDTO.Country,
            RunwaysCount = airportDTO.RunwaysCount,
            City = airportDTO.City,
        };

        await this._airportRepository.Add(airportToCreate);
        await this._airportRepository.Complete();
    }
    public async Task UpdateAirport(AirportDTO updatedAirportDTO, string airportCode)
    {
        //this._airportRepository.Update(this._autoMapper.Map<Airport>(updatedAirport));
        Airport? airport = await this._airportRepository.SingleOrDefault(a => a.Code == airportCode);
        if (airport == null)
        {
            throw new NotFoundException($"Airport with airport code {airportCode} does not persist");
        }

        airport.City = updatedAirportDTO.City;
        airport.Country = updatedAirportDTO.Country;
        airport.Name = updatedAirportDTO.Name;
        airport.RunwaysCount = updatedAirportDTO.RunwaysCount;
        airport.Founded = updatedAirportDTO.Founded;

        await this._airportRepository.Complete();
    }
    public async Task DeleteAirport(string code)
    {
        Airport? airportToRemove = await this._airportRepository.SingleOrDefault(a => a.Code == code);
        if (airportToRemove == null)
        {
            throw new NotFoundException($"Airport with code {code} does not persist");
        }

        this._airportRepository.Remove(airportToRemove);
        await this._airportRepository.Complete();
    }
}
