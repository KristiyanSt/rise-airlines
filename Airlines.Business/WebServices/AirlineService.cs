﻿using Airlines.Business.Interfaces;
using Airlines.Business.DTOs;
using Airlines.Persistence.Basic.Interfaces;
using Airlines.Persistence.Basic.Entities;
using System.Linq.Expressions;
using Airlines.Business.Exceptions;

namespace Airlines.Business.WebServices;
public class AirlineService : IAirlineService
{
    private IAirlineRepository _airlineRepository;
    public AirlineService(IAirlineRepository airlineRepository)
    {
        this._airlineRepository = airlineRepository;
    }

    public IQueryable<Airline> GetAll()
    {
        return this._airlineRepository.GetAll();
    }
    public IQueryable<Airline> Find(Expression<Func<Airline, bool>> predicate)
    {
        return this._airlineRepository.Find(predicate);
    }
    public async Task<Airline?> GetAirlineByName(string name)
    {
        return await this._airlineRepository.SingleOrDefault(a => a.Name == name);
    }
    public async Task AddAirline(AirlineDTO airlineDTO)
    {
        Airline? existingAirline = await this._airlineRepository.SingleOrDefault(a => a.Name == airlineDTO.Name);
        if (existingAirline != null)
        {
            throw new RecordAlreadyExistsException($"Airline with {airlineDTO.Name} name already exists");
        }

        Airline airlineToCreate = new Airline
        {
            Name = airlineDTO.Name,
            Founded = airlineDTO.Founded,
            FleetSize = airlineDTO.FleetSize,
            Description = airlineDTO.Description
        };

        await this._airlineRepository.Add(airlineToCreate);
        await this._airlineRepository.Complete();
    }
    public async Task UpdateAirline(AirlineDTO updatedAirlineDTO, string airlineName)
    {
        //this._airlineRepository.Update(this._autoMapper.Map<Airline>(updatedAirline));
        Airline? airline = await this._airlineRepository.SingleOrDefault(a => a.Name == airlineName);
        if (airline == null)
        {
            throw new NotFoundException($"Airline with {airlineName} name does not persist");
        }

        airline.Description = updatedAirlineDTO.Description;
        airline.FleetSize = updatedAirlineDTO.FleetSize;
        airline.Founded = updatedAirlineDTO.Founded;

        await this._airlineRepository.Complete();
    }
    public async Task DeleteAirline(string name)
    {
        Airline? airlineToRemove = await this._airlineRepository.SingleOrDefault(a => a.Name == name);
        if (airlineToRemove == null)
        {
            throw new NotFoundException($"Airline with {name} name does not persist");
        }

        this._airlineRepository.Remove(airlineToRemove);
        await this._airlineRepository.Complete();
    }
}
