﻿using Airlines.Business.DataStructures;
using Airlines.Business.Exceptions;
using Airlines.Business.Facilities;

namespace Airlines.Business.Strategies;
public class ShortestRouteStrategy : IStrategy
{
    private AirportVertex? FindAirportVertexByAirportId(List<AirportVertex> airportVertices, string airportId)
    {
        return airportVertices.Find(airportVertex => string.Equals(airportVertex.Airport.Identifier, airportId, StringComparison.OrdinalIgnoreCase));
    }

    public Route? BuildRoute(List<AirportVertex> airportVertices, string startAirportId, string endAirportId)
    {
        AirportVertex? startAirportVertex = this.FindAirportVertexByAirportId(airportVertices, startAirportId);
        AirportVertex? endAirportVertex = this.FindAirportVertexByAirportId(airportVertices, endAirportId);

        if (startAirportVertex == null)
        {
            throw new InvalidInputException($"Invalid airport {startAirportId}");
        }
        if (endAirportVertex == null)
        {
            throw new InvalidInputException($"Invalid airport {endAirportId}");
        }

        // Queue for BFS
        Queue<AirportVertex> queue = new Queue<AirportVertex>();

        // List to store the path
        Dictionary<string, Flight> prev = new Dictionary<string, Flight>();

        // HashSet to mark visited vertices' airports
        HashSet<string> visited = new HashSet<string>();

        queue.Enqueue(startAirportVertex);
        visited.Add(startAirportVertex.Airport.Identifier);

        while (queue.Count > 0)
        {
            AirportVertex currentAirportVertex = queue.Dequeue();
            foreach (Flight flight in currentAirportVertex.Flights)
            {
                if (!visited.Any(airportId => string.Equals(airportId, flight.ArrivalAirport, StringComparison.OrdinalIgnoreCase)))
                {
                    AirportVertex? neighborAirportVertex = this.FindAirportVertexByAirportId(airportVertices, flight.ArrivalAirport);
                    if (neighborAirportVertex != null)
                    {
                        queue.Enqueue(neighborAirportVertex);
                        visited.Add(neighborAirportVertex.Airport.Identifier);
                        prev[neighborAirportVertex.Airport.Identifier] = flight;
                    }
                }
            }
        }

        // Construct the shortest path
        Route shortestPathRoute = new Route();
        for (string at = endAirportId; at != null; at = prev[at].DepartureAirport)
        {
            if (!prev.ContainsKey(at))
            {
                return null;
            }
            shortestPathRoute.PrependFlight(prev[at]);
            if (string.Equals(startAirportId, prev[at].DepartureAirport, StringComparison.OrdinalIgnoreCase))
            {
                break;
            }
        }

        return shortestPathRoute;
    }
}
