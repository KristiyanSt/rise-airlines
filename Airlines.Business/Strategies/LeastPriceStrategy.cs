﻿using Airlines.Business.DataStructures;
using Airlines.Business.Exceptions;
using Airlines.Business.Facilities;

namespace Airlines.Business.Strategies;
public class LeastPriceStrategy : IStrategy
{
    private AirportVertex? FindAirportVertexByAirportId(List<AirportVertex> airportVertices, string airportId)
    {
        return airportVertices.Find(airportVertex => string.Equals(airportVertex.Airport.Identifier, airportId, StringComparison.OrdinalIgnoreCase));
    }

    public Route? BuildRoute(List<AirportVertex> airportVertices, string startAirportId, string endAirportId)
    {
        AirportVertex? startAirportVertex = this.FindAirportVertexByAirportId(airportVertices, startAirportId);
        AirportVertex? endAirportVertex = this.FindAirportVertexByAirportId(airportVertices, endAirportId);

        if (startAirportVertex == null)
        {
            throw new InvalidInputException($"Invalid airport {startAirportId}");
        }
        if (endAirportVertex == null)
        {
            throw new InvalidInputException($"Invalid airport {endAirportId}");
        }

        Dictionary<AirportVertex, double> cheapestPrices = new Dictionary<AirportVertex, double>();
        Dictionary<AirportVertex, Flight?> previousFlight = new Dictionary<AirportVertex, Flight?>();
        List<AirportVertex> unvisitedAirports = new List<AirportVertex>();

        foreach (AirportVertex vertex in airportVertices)
        {
            cheapestPrices[vertex] = double.MaxValue;
            previousFlight[vertex] = null;
            unvisitedAirports.Add(vertex);
        }

        cheapestPrices[startAirportVertex] = 0;

        while (unvisitedAirports.Any())
        {
            AirportVertex currentVertex = unvisitedAirports.OrderBy(v => cheapestPrices[v]).First();

            if (string.Equals(currentVertex.Airport.Identifier, endAirportId, StringComparison.OrdinalIgnoreCase))
            {
                break;
            }

            unvisitedAirports.Remove(currentVertex);

            foreach (Flight flight in currentVertex.Flights)
            {
                AirportVertex? neighborVertex = this.FindAirportVertexByAirportId(airportVertices, flight.ArrivalAirport);

                if (neighborVertex == null)
                {
                    continue;
                }

                double priceToNeighbor = cheapestPrices[currentVertex] + flight.Price;

                if (priceToNeighbor < cheapestPrices[neighborVertex])
                {
                    cheapestPrices[neighborVertex] = priceToNeighbor;
                    previousFlight[neighborVertex] = flight;
                }
            }
        }

        Route route = new Route();
        AirportVertex current = endAirportVertex;

        if (previousFlight.ContainsKey(current))
        {
            while (previousFlight[current!] != null)
            {
                route.PrependFlight(previousFlight[current!]!);
                current = this.FindAirportVertexByAirportId(airportVertices, previousFlight[current!]!.DepartureAirport)!;
            }
        }
        else
        {
            return null;
        }
        return route;
    }
}
