﻿using Airlines.Business.DataStructures;
using Airlines.Business.Facilities;

namespace Airlines.Business.Strategies;
public interface IStrategy
{
    public Route? BuildRoute(List<AirportVertex> airportVertices, string startAirportId, string endAirportId);
}
