﻿using System.Collections;

namespace Airlines.Business.DataStructures;
public interface ISingleLinkedList<T>
{
    uint Count { get; }
    void AddFirst(T item);
    void AddLast(T item);
    bool RemoveFirst();
    bool RemoveLast();
    bool Contains(T item);
}

public class Node<T>
{
    public T Value
    {
        get;
        set;
    }

    public Node<T>? Next
    {
        get;
        set;
    }

    public Node(T value, Node<T>? next = null)
    {
        Value = value;
        Next = next;
    }
}
public class SingleLinkedList<T> : IEnumerable<T>, ISingleLinkedList<T>
{


    public uint length;
    public uint Count => this.length;
    Node<T>? Head
    {
        get;
        set;
    }
    Node<T>? Tail
    {
        get;
        set;
    }

    public SingleLinkedList()
    {
        this.length = 0;
    }

    public void AddFirst(T item)
    {
        var newNode = new Node<T>(item);
        if (this.length == 0)
        {
            this.Head = newNode;
            this.Tail = newNode;
        }
        else
        {
            newNode.Next = this.Head;
            this.Head = newNode;
        }

        this.length++;
    }

    public void AddLast(T item)
    {
        var newNode = new Node<T>(item);
        if (this.length == 0)
        {
            this.Head = newNode;
            this.Tail = newNode;
        }
        else
        {
            this.Tail.Next = newNode;
            this.Tail = newNode;
        }
        this.length++;
    }
    public Node<T>? GetFirst()
    {
        return this.Head;
    }
    public Node<T>? GetLast()
    {
        return this.Tail;
    }

    public bool Contains(T item)
    {
        if (this.Count == 0)
        {
            return false;
        }
        var currentNode = this.Head;
        while (true)
        {
            if (currentNode.Value.Equals(item))
            {
                return true;
            }
            if (currentNode.Next == null)
            {
                break;
            }
            currentNode = currentNode.Next;
        }
        return false;
    }

    public bool RemoveFirst()
    {
        if (this.length == 0)
        {
            return false;
        }
        if (this.length == 1)
        {
            this.Head = null;
            this.Tail = null;
            this.length--;
            return true;
        }
        this.Head = this.Head.Next;
        this.length--;
        return true;
    }

    public bool RemoveLast()
    {
        if (this.Count == 0)
        {
            return false;
        }
        if (length == 1)
        {
            Head = null;
            Tail = null;
            this.length--;
            return true;
        }
        Node<T>? beforeLast = null;
        var last = Head;
        while (last != null)
        {
            if (last.Next == null)
            {
                break;
            }

            beforeLast = last;
            last = last.Next;
        }

        beforeLast!.Next = null;
        Tail = beforeLast;
        length--;

        return true;
    }

    public List<string> ToStringList()
    {
        List<string> listData = new List<string>();
        var currentNode = this.Head;
        if (currentNode != null)
        {
            if (this.Count > 0)
            {
                while (true)
                {
                    if (!string.IsNullOrEmpty(currentNode.Value.ToString()))
                    {
                        listData.Add(currentNode.Value.ToString()!);
                        if (currentNode.Next == null)
                        {
                            break;
                        }
                        currentNode = currentNode.Next;
                    }
                }
            }
        }
        return listData;
    }

    public IEnumerator<T> GetEnumerator()
    {
        Node<T>? current = Head;
        while (current != null)
        {
            yield return current.Value;
            current = current.Next;
        }
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }
}


