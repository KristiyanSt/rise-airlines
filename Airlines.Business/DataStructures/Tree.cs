﻿using Airlines.Business.Facilities;
using Airlines.Business.Services;
using System.ComponentModel.DataAnnotations;

namespace Airlines.Business.DataStructures;

public class TreeNode
{
    public Airport Airport
    {
        get;
        set;
    }
    public Flight Flight
    {
        get;
        set;
    }
    public List<TreeNode> Children { get; } = new List<TreeNode>();
    public TreeNode(Airport airport, Flight flight)
    {
        this.Airport = airport;
        this.Flight = flight;
    }
    public TreeNode(Airport airport)
    {
        this.Airport = airport;
    }
}
public class Tree
{
    public TreeNode Root
    {
        get;
        set;
    }

    public Tree(TreeNode rootNode)
    {
        Root = rootNode;
    }

    public bool TryToConnect(TreeNode currentNode, Flight flight)
    {
        if (currentNode.Airport.Identifier == flight.DepartureAirport)
        {
            Airport arrivalAirport = AirportService.GetAirportById(flight.ArrivalAirport);
            currentNode.Children.Add(new TreeNode(arrivalAirport, flight));
            return true;
        }

        foreach (TreeNode child in currentNode.Children)
        {
            bool isConnected = TryToConnect(child, flight);
            if (isConnected)
            {
                return true;
            }
        }

        return false;
    }

    public Route? SearchForRoute(string destinationAirport, TreeNode currentNode, Route? currentRoute = null)
    {
        if (currentRoute == null)
        {
            currentRoute = new Route();
        }
        if (currentNode.Flight != null)
        {
            currentRoute.AppendFlight(currentNode.Flight);

            if (currentNode.Airport.Identifier.ToLower() == destinationAirport.ToLower())
            {
                return currentRoute;
            }

            foreach (var childNode in currentNode.Children)
            {
                Route? route = SearchForRoute(destinationAirport, childNode, currentRoute);
                if (route != null)
                {
                    return route;
                }
            }

            currentRoute.RemoveLastFlight();
        }
        else
        {
            foreach (var childNode in currentNode.Children)
            {
                Route? route = SearchForRoute(destinationAirport, childNode, currentRoute);
                if (route != null)
                {
                    return route;
                }
            }
        }
        return null;
    }
}
