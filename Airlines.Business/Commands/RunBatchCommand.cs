﻿namespace Airlines.Business.Commands;
public class RunBatchCommand : ICommand
{
    private Batch _batch;
    public RunBatchCommand(Batch batch)
    {
        this._batch = batch;
    }

    public void Execute()
    {
        this._batch.RunBatch();
    }
}
