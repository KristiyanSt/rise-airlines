﻿using Airlines.Business.Services;

namespace Airlines.Business.Commands;
public class ExistCommand : ICommand
{
    private string _query;
    public ExistCommand(string query)
    {
        this._query = query;
    }
    public void Execute()
    {
        string result = AirlineService.IsAirlineExisting(this._query);
        PrintHandler.DisplayMessage(result);
    }
}
