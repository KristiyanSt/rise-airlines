﻿using Airlines.Business.Services;

namespace Airlines.Business.Commands;
public class ReserveCargoCommand : ICommand
{
    private string _flightId;
    private string _cargoWeight;
    private string _cargoVolume;
    public ReserveCargoCommand(string flightId, string cargoWeight, string cargoVolume)
    {
        this._flightId = flightId;
        this._cargoWeight = cargoWeight;
        this._cargoVolume = cargoVolume;
    }
    public void Execute()
    {
        string resultMessage = AircraftService.ReserveCargo(this._flightId, this._cargoWeight, this._cargoVolume);
        PrintHandler.DisplayMessage(resultMessage);
    }
}
