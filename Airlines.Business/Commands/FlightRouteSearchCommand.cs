﻿using Airlines.Business.DatabaseNS;
using Airlines.Business.DataStructures;
using Airlines.Business.Facilities;

namespace Airlines.Business.Commands;
public class FlightRouteSearchCommand : ICommand
{
    private string _destinationAirport;
    public FlightRouteSearchCommand(string destinationAirport)
    {
        this._destinationAirport = destinationAirport;
    }
    public void Execute()
    {
        Database db = Database.GetInstance();
        Tree? flightRoutesTree = db.GetFlightRoutesTree();
        if(flightRoutesTree == null)
        {

        }
        Route? flightRoute = flightRoutesTree.SearchForRoute(this._destinationAirport, flightRoutesTree.Root);
        if (flightRoute != null)
        {
            string typeOfFlightRoute = flightRoute.GetCount() == 1
                ? "Direct flight route"
                : "Indirect flight route";
            PrintHandler.DisplayListData(flightRoute.ToStringList(), typeOfFlightRoute);
        }
        else
        {
            PrintHandler.DisplayMessage("No flight route has been found");
        }
    }
}
