﻿using Airlines.Business.Services;

namespace Airlines.Business.Commands;
public class NewRouteCommand : ICommand
{
    public void Execute()
    {
        RouteService.AddRoute();
    }
}
