﻿using Airlines.Business.DataStructures;
using Airlines.Business.Facilities;
using Airlines.Business.Services;
using Airlines.Business.Strategies;

namespace Airlines.Business.Commands;
public class RouteCheckCommand : ICommand
{
    private string _startAirportId;
    private string _endAirportId;
    public RouteCheckCommand(string startAirportId, string endAirportId)
    {
        this._startAirportId = startAirportId;
        this._endAirportId = endAirportId;
    }
    public void Execute()
    {
        FlightNetwork flightNetwork = FlightNetworkService.GetFlightNetwork();
        flightNetwork.SetStrategy(new ShortestRouteStrategy());
        Route? connectionRoute = flightNetwork.ExecuteStrategy(this._startAirportId, this._endAirportId);

        string resultMessage;
        if (connectionRoute == null)
        {
            resultMessage = "Airports are not connected";
        }
        else
        {
            string typeOfConnection = connectionRoute.GetCount() == 1
                ? "directly"
                : "through intermediate stops";

            resultMessage = "Airports are connected" + " " + typeOfConnection;
        }
        PrintHandler.DisplayMessage(resultMessage);
    }
}
