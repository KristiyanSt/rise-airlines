﻿using Airlines.Business.Exceptions;

namespace Airlines.Business.Commands;
public class CommandFactory
{
    public static ICommand CreateCommand(string[] commandArgs, Batch batch)
    {
        if (commandArgs.Length < 2 && commandArgs.Length > 6)
        {
            throw GetInvalidCommandException();
        }

        string firstArg = commandArgs[0].Trim();
        string secondArg = commandArgs[1].Trim();

        if (commandArgs.Length == 2)
        {
            string query = commandArgs[1].Trim();

            if (firstArg == "search")
            {
                return new SearchCommand(query);
            }
            else if (firstArg == "exist")
            {
                return new ExistCommand(query);
            }
            else if (firstArg == "route" && secondArg == "new")
            {
                return new NewRouteCommand();
            }
            else if (firstArg == "route" && secondArg == "remove")
            {
                return new RemoveLastInRouteCommand();
            }
            else if (firstArg == "route" && secondArg == "print")
            {
                return new RoutePrintCommand();
            }
            else if (firstArg == "batch" && secondArg == "start")
            {
                return new StartBatchCommand(batch);
            }
            else if (firstArg == "batch" && secondArg == "run")
            {
                return new RunBatchCommand(batch);
            }
            else if (firstArg == "batch" && secondArg == "cancel")
            {
                return new CancelBatchCommand(batch);
            }

            throw GetInvalidCommandException();
        }
        else if (commandArgs.Length == 3)
        {
            if (firstArg == "sort")
            {
                string facilityType = commandArgs[1];
                string orderBy = commandArgs[2];
                return new SortCommand(facilityType, orderBy);
            }
            else if (firstArg == "list")
            {
                string inputData = commandArgs[1];
                string criteria = commandArgs[2];
                return new ListCommand(inputData, criteria);
            }
            else if (firstArg == "route" && secondArg == "add")
            {
                string flightId = commandArgs[2];
                return new AddToRouteCommand(flightId);
            }
            else if (firstArg == "route" && secondArg == "find")
            {
                string destination = commandArgs[2];
                return new FlightRouteSearchCommand(destination);
            }

            throw GetInvalidCommandException();
        }
        else if (commandArgs.Length == 4)
        {
            if (firstArg == "route" && secondArg == "check")
            {
                string startAirportId = commandArgs[2];
                string endAirportId = commandArgs[3];
                return new RouteCheckCommand(startAirportId, endAirportId);
            }
            else if (firstArg == "route" && secondArg == "search")
            {
                string startAirportId = commandArgs[2];
                string endAirportId = commandArgs[3];
                return new RouteSearchCommand(startAirportId, endAirportId);
            }

            throw GetInvalidCommandException();
        }
        else if (commandArgs.Length == 5)
        {
            if (firstArg == "reserve" && secondArg == "cargo")
            {
                string flightId = commandArgs[2];
                string cargoWeight = commandArgs[3];
                string cargoVolume = commandArgs[4];
                return new ReserveCargoCommand(flightId, cargoWeight, cargoVolume);
            }
            else if (firstArg == "route" && secondArg == "search")
            {
                string startAirportId = commandArgs[2];
                string endAirportId = commandArgs[3];
                string strategy = commandArgs[4];
                return new AdvancedRouteSearchCommand(startAirportId, endAirportId, strategy);
            }

            throw GetInvalidCommandException();
        }
        else if (commandArgs.Length == 6)
        {
            if (firstArg == "reserve" && secondArg == "ticket")
            {
                string flightId = commandArgs[2];
                string seats = commandArgs[3];
                string smallBaggageCount = commandArgs[4];
                string largeBaggageCount = commandArgs[5];
                return new ReserveTicketCommand(flightId, seats, smallBaggageCount, largeBaggageCount);
            }

            throw GetInvalidCommandException();
        }

        throw GetInvalidCommandException();
    }

    private static Exception GetInvalidCommandException()
    {
        return new InvalidCommandException();
    }
}
