﻿using Airlines.Business.Services;

namespace Airlines.Business.Commands;
public class RemoveLastInRouteCommand : ICommand
{
    public void Execute()
    {
        RouteService.RemoveLastInRoute();
    }
}
