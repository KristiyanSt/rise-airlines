﻿using Airlines.Business.Facilities;
using Airlines.Business.Services;

namespace Airlines.Business.Commands;
public class ListCommand : ICommand
{
    private string _facilityType;
    private string _criteria;
    public ListCommand(string facilityType, string criteria)
    {
        this._facilityType = facilityType;
        this._criteria = criteria;
    }

    public void Execute()
    {
        List<Airport> airportsByCriteria = AirportService.GetAirportsByCriteria(this._facilityType, this._criteria);
        PrintHandler.DisplayListData(airportsByCriteria, $"Airports in {this._facilityType}: ");
    }
}
