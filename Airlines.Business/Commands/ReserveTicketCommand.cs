﻿using Airlines.Business.Services;

namespace Airlines.Business.Commands;
public class ReserveTicketCommand : ICommand
{
    private string _flightId;
    private string _seats;
    private string _smallBaggageCount;
    private string _largeBaggageCount;
    public ReserveTicketCommand(string flightId, string seats, string smallBaggageCount, string largeBaggageCount)
    {
        this._flightId = flightId;
        this._seats = seats;
        this._smallBaggageCount = smallBaggageCount;
        this._largeBaggageCount = largeBaggageCount;
    }
    public void Execute()
    {
        string resultMessage = AircraftService.ReserveTicket(this._flightId, this._seats, this._smallBaggageCount, this._largeBaggageCount);
        PrintHandler.DisplayMessage(resultMessage);
    }
}
