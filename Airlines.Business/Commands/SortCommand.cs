﻿using Airlines.Business.Services;

namespace Airlines.Business.Commands;
public class SortCommand : ICommand
{
    private string _facilityType;
    private string _orderBy;

    public SortCommand(string facilityType, string orderBy)
    {
        this._facilityType = facilityType;
        this._orderBy = orderBy;
    }
    public void Execute()
    {
        (List<string> facilitiesList, string facilityType) = CommonService.SortDataByOrder(this._facilityType, this._orderBy);
        PrintHandler.DisplayListData(facilitiesList, facilityType);
    }
}
