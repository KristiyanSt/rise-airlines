﻿using Airlines.Business.Services;

namespace Airlines.Business.Commands;
public class RoutePrintCommand : ICommand
{
    public void Execute()
    {
        List<string> routeList = RouteService.GetRoute();
        PrintHandler.DisplayListData(routeList, "Route");
    }
}
