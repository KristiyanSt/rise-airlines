﻿namespace Airlines.Business.Commands;
public class CancelBatchCommand : ICommand
{
    private Batch _batch;
    public CancelBatchCommand(Batch batch)
    {
        this._batch = batch;
    }
    public void Execute()
    {
        this._batch.CancelBatch();
    }
}

