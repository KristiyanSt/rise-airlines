﻿using Airlines.Business.DataStructures;
using Airlines.Business.Facilities;
using Airlines.Business.Services;
using Airlines.Business.Strategies;

namespace Airlines.Business.Commands;
public class RouteSearchCommand : ICommand
{
    private string _startAirportId;
    private string _endAirportId;
    public RouteSearchCommand(string startAirportId, string endAirportId)
    {
        this._startAirportId = startAirportId;
        this._endAirportId = endAirportId;
    }
    public void Execute()
    {
        FlightNetwork flightNetwork = FlightNetworkService.GetFlightNetwork();
        flightNetwork.SetStrategy(new ShortestRouteStrategy());
        Route? connectionRoute = flightNetwork.ExecuteStrategy(this._startAirportId, this._endAirportId);

        if (connectionRoute == null)
        {
            PrintHandler.DisplayMessage("Airports are not connected");
        }
        else
        {
            PrintHandler.DisplayListData(connectionRoute.ToStringList(), "Shortest path");
        }
    }
}
