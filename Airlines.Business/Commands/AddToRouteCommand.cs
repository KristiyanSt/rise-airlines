﻿using Airlines.Business.Services;

namespace Airlines.Business.Commands;
public class AddToRouteCommand : ICommand
{
    private string _flightId;
    public AddToRouteCommand(string flightId)
    {
        this._flightId = flightId;
    }
    public void Execute()
    {
        RouteService.AddToRoute(this._flightId);
    }
}