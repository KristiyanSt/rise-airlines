﻿namespace Airlines.Business.Commands;
public class StartBatchCommand : ICommand
{
    private Batch _batch;
    public StartBatchCommand(Batch batch)
    {
        this._batch = batch;
    }
    public void Execute()
    {
        this._batch.StartBatch();
    }
}
