﻿namespace Airlines.Business.Commands;
public class Batch
{
    private bool _batchEnabled;
    private Queue<ICommand> _commandsQueue;
    public Batch()
    {
        this._batchEnabled = false;
        this._commandsQueue = new Queue<ICommand>();
    }
    public bool IsBatchEnabled()
    {
        return this._batchEnabled;
    }
    public bool StartBatch()
    {
        this._batchEnabled = true;
        return true;
    }
    public bool CancelBatch()
    {
        this._batchEnabled = false;
        ICommand? command;
        while (this._commandsQueue.TryDequeue(out command))
        {
        }
        return true;
    }

    public bool RunBatch()
    {
        try
        {
            this._batchEnabled = false;
            ICommand? command;
            while (this._commandsQueue.TryDequeue(out command))
            {
                command.Execute();
            }
        }
        catch (Exception e)
        {
            PrintHandler.DisplayMessage(e.Message);
        }

        return true;
    }

    public bool AddToBatch(ICommand command)
    {
        this._commandsQueue.Enqueue(command);
        return true;
    }
}
