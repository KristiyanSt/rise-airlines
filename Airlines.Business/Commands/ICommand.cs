﻿namespace Airlines.Business.Commands;
public interface ICommand
{
    public void Execute();
}
