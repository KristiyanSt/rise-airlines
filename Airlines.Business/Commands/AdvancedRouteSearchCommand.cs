﻿using Airlines.Business.DataStructures;
using Airlines.Business.Exceptions;
using Airlines.Business.Facilities;
using Airlines.Business.Services;
using Airlines.Business.Strategies;

namespace Airlines.Business.Commands;
public class AdvancedRouteSearchCommand : ICommand
{
    private string _startAirportId;
    private string _endAirportId;
    private string _strategy;
    public AdvancedRouteSearchCommand(string startAirportId, string endAirportId, string strategy)
    {
        this._startAirportId = startAirportId;
        this._endAirportId = endAirportId;
        this._strategy = strategy;
    }
    public void Execute()
    {
        FlightNetwork flightNetwork = FlightNetworkService.GetFlightNetwork();
        string typeOfStrategy;

        switch (this._strategy)
        {
            case "cheap":
                flightNetwork.SetStrategy(new LeastPriceStrategy());
                typeOfStrategy = "Cheapest route";
                break;
            case "short":
                flightNetwork.SetStrategy(new LeastTimeStrategy());
                typeOfStrategy = "Least time route";
                break;
            case "stops":
                flightNetwork.SetStrategy(new ShortestRouteStrategy());
                typeOfStrategy = "Shortest route";
                break;
            default:
                throw new InvalidInputException($"Invalid strategy {this._strategy}, possible choices are 'cheap', 'short' and 'stops'");
        }

        Route? connectionRoute = flightNetwork.ExecuteStrategy(this._startAirportId, this._endAirportId);

        if (connectionRoute == null)
        {
            PrintHandler.DisplayMessage("Airports are not connected");
        }
        else
        {
            PrintHandler.DisplayListData(connectionRoute.ToStringList(), typeOfStrategy);
        }
    }
}
