﻿using Airlines.Business.Services;

namespace Airlines.Business.Commands;
public class SearchCommand : ICommand
{
    private string _query;
    public SearchCommand(string query)
    {
        this._query = query;
    }
    public void Execute()
    {
        string resultItem = CommonService.PerformSearch(this._query);
        PrintHandler.DisplayMessage($"Searched item is {resultItem}");
    }
}
