﻿using Airlines.Business.DTOs;
using Airlines.Persistence.Basic.Entities;
using System.Linq.Expressions;

namespace Airlines.Business.Interfaces;
public interface IAirportService
{
    IQueryable<Airport> GetAll();
    IQueryable<Airport> Find(Expression<Func<Airport, bool>> predicate);
    IQueryable<Airport> GetAirportsByCity(string city);
    Task<Airport?> GetAirportByCode(string code);
    Task AddAirport(AirportDTO airportDTO);
    Task UpdateAirport(AirportDTO updatedAirportDTO, string airportCode);
    Task DeleteAirport(string code);
}
