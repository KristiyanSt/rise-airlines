﻿using Airlines.Business.DTOs;
using Airlines.Persistence.Basic.Entities;
using System.Linq.Expressions;

namespace Airlines.Business.Interfaces;
public interface IAirlineService
{
    IQueryable<Airline> GetAll();
    IQueryable<Airline> Find(Expression<Func<Airline, bool>> predicate);
    Task<Airline?> GetAirlineByName(string name);
    Task AddAirline(AirlineDTO airlineDTO);
    Task UpdateAirline(AirlineDTO updatedAirlineDTO, string airlineName);
    Task DeleteAirline(string name);
}
