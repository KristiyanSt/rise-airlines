﻿using Airlines.Persistence.Basic.Entities;
using Airlines.Business.DTOs;
using System.Linq.Expressions;

namespace Airlines.Business.Interfaces;
public interface IFlightService
{
    IQueryable<Flight> GetAll();
    IQueryable<Flight> Find(Expression<Func<Flight, bool>> predicate);
    Task<Flight?> GetFlightById(int id);
    Task<Flight?> GetFlightWithAirports(int id);
    IQueryable<Flight> FindDepartingFlightsByAirport(string airport);
    Task AddFlight(FlightDTO flightDTO);
    Task UpdateFlight(FlightDTO updatedFlightDTO, int id);
    Task DeleteFlight(int id);
}
