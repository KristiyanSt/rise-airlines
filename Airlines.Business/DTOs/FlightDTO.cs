﻿using System.ComponentModel.DataAnnotations;

namespace Airlines.Business.DTOs;
public class FlightDTO
{
    public int Id { get; set; }
    [Required(ErrorMessage = "Flight number is required")]
    public string FlightNumber { get; set; }
    [Required(ErrorMessage = "Departure airport code is required")]
    public string DepartureAirport { get; set; }
    [Required(ErrorMessage = "Arrival airport code is required")]
    public string ArrivalAirport { get; set; }
    [Required(ErrorMessage = "Departure date is required")]
    public DateTime DepartureDateTime { get; set; }
    [Required(ErrorMessage = "Arrival date is required")]
    public DateTime ArrivalDateTime { get; set; }
}
