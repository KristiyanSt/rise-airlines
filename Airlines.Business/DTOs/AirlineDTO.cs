﻿using System.ComponentModel.DataAnnotations;

namespace Airlines.Business.DTOs;
public class AirlineDTO
{
    [Required(ErrorMessage = "Airline name is required")]
    [MaxLength(5, ErrorMessage = "Airline name must be 5 symbols maximum")]
    public string Name { get; set; }
    [Required(ErrorMessage = "Founded date is required")]
    public DateTime Founded { get; set; }
    [Required(ErrorMessage = "Fleet size is required")]
    public int FleetSize { get; set; }

    [Required(ErrorMessage = "Description is required")]
    [MaxLength(255)]
    public string Description { get; set; }
}