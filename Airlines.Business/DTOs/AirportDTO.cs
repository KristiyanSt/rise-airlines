﻿using System.ComponentModel.DataAnnotations;

namespace Airlines.Business.DTOs;
public class AirportDTO
{
    [Required(ErrorMessage = "Airport code is required")]
    [Range(3, 3, ErrorMessage = "Airport code must be exactly 3 characters")]
    public string Code { get; set; }
    [Required(ErrorMessage = "Airport name is required")]
    public string Name { get; set; }
    [Required(ErrorMessage = "Airport city is required")]
    public string City { get; set; }
    [Required(ErrorMessage = "Airport country is required")]
    public string Country { get; set; }
    [Required(ErrorMessage = "Runways count is required")]
    public int RunwaysCount { get; set; }
    [Required(ErrorMessage = "Founded date is required")]
    public DateTime Founded { get; set; }
}
