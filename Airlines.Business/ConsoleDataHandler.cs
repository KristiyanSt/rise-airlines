﻿using Airlines.Business.Utils;
using Airlines.Business.Services;
using Airlines.Business.Exceptions;

namespace Airlines.Business;

public class ConsoleDataHandler

{
    private CommandHandler _commandHandler;

    public ConsoleDataHandler(CommandHandler commandHandler)
    {
        this._commandHandler = commandHandler;
    }
    public void CollectConsoleData()
    {
        this.ContinuouslyReadAndProcessConsoleInput("airports");
        this.ContinuouslyReadAndProcessConsoleInput("airlines");
        this.ContinuouslyReadAndProcessConsoleInput("flights");
    }
    private void ContinuouslyReadAndProcessConsoleInput(string facilityType)
    {
        PrintHandler.DisplayMessage($"Enter {facilityType} below, to continue enter \"continue\"");
        while (true)
        {
            try
            {
                var input = Console.ReadLine();
                input = Validation.ValidateString(input);

                if (input == "continue")
                {
                    break;
                }
                string[] fields;
                switch (facilityType)
                {
                    case "airport":
                        fields = input.Split(", ", StringSplitOptions.RemoveEmptyEntries);
                        if (fields.Length != 4)
                        {
                            throw new InvalidInputException("Invalid input elements");
                        }
                        string identifier = fields[0];
                        string name = fields[1];
                        string city = fields[2];
                        string country = fields[3];
                        AirportService.AddAirport(identifier, name, city, country);
                        break;
                    case "airline":
                        string airlineName = input;
                        AirlineService.AddAirline(airlineName);
                        break;
                    case "flight":
                        fields = input.Split(",", StringSplitOptions.RemoveEmptyEntries);
                        if (fields.Length != 6)
                        {
                            throw new InvalidInputException("Invalid input elements");
                        }
                        string flightId = fields[0];
                        string departureAirport = fields[1];
                        string arrivalAirport = fields[2];
                        string aircraftModel = fields[3];
                        string price = fields[4];
                        string time = fields[5];
                        FlightService.AddFlight(flightId, departureAirport, arrivalAirport, aircraftModel, price, time);
                        break;
                }
            }
            catch (Exception e)
            {
                PrintHandler.DisplayMessage(e.Message);
            }
        }
    }
    public void ContinuouslyReadAndDelegateCommandInput()
    {
        while (true)
        {
            try
            {
                var input = Console.ReadLine();
                input = Validation.ValidateString(input);

                if (input == "finish")
                {
                    break;
                }

                string[] commandArgs = input.Split(" ", StringSplitOptions.RemoveEmptyEntries);
                this._commandHandler.HandleCommandArgs(commandArgs);
            }
            catch (Exception e)
            {
                PrintHandler.DisplayMessage(e.Message);
            }
        }
    }
}

