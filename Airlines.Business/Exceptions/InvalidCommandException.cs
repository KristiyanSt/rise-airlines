﻿namespace Airlines.Business.Exceptions;
public class InvalidCommandException : Exception
{
    public InvalidCommandException() : base("Invalid command")
    {
    }
}

