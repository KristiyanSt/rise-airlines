﻿using Airlines.Business.Commands;

namespace Airlines.Business;
public class CommandHandler
{
    private Batch _batch;
    public CommandHandler()
    {
        _batch = new Batch();
    }
    public void HandleCommandArgs(string[] commandArgs)
    {
        ICommand generatedCommand = CommandFactory.CreateCommand(commandArgs, _batch);

        if (generatedCommand is StartBatchCommand && _batch.IsBatchEnabled())
        {
            throw new ArgumentException("Batch mode is already started");
        }
        else if (generatedCommand is RunBatchCommand && !_batch.IsBatchEnabled() ||
            generatedCommand is CancelBatchCommand && !_batch.IsBatchEnabled())
        {
            throw new ArgumentException("Batch mode is not started");
        }

        if (generatedCommand is RunBatchCommand ||
            generatedCommand is CancelBatchCommand)
        {
            generatedCommand.Execute();
        }

        if (_batch.IsBatchEnabled())
        {
            _batch.AddToBatch(generatedCommand);
        }
        else
        {
            generatedCommand.Execute();
        }
    }
}
