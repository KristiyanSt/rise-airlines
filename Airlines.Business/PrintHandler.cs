﻿namespace Airlines.Business;
public static class PrintHandler
{
    public static void DisplayListData<T>(List<T> data, string? listDesc)
    {
        if (listDesc != null)
        {
            Console.Write(listDesc + ": ");
        }
        Console.Write(string.Join(", ", data.Select(x => x.ToString())));
        Console.WriteLine();
    }
    public static void DisplayMessage(string message)
    {
        Console.WriteLine(message);
    }

    public static void DisplayCommandInfo()
    {
        //System.Console.WriteLine("Searching: type \"search <search term>\"");
        //System.Console.WriteLine("Sorting: type \"sort <input data> [order]\" (order is optional)");
        //System.Console.WriteLine("Existing?: type \"exist <airline name>\"");
        //System.Console.WriteLine("Listing: type \"list <input data> <from>\"");
        //System.Console.WriteLine("Create route: type \"route new\"");
        //System.Console.WriteLine("Add flight to route: type \"route add <flight identifier>\"");
        //System.Console.WriteLine("Remove last route: type \"route remove\"");
        //System.Console.WriteLine("Print route: type \"route print\"");
        //System.Console.WriteLine("Reserve cargo: type \"reserve cargo <flight identifier> <cargo weight> <cargo volume>\"");
        //System.Console.WriteLine("Reserve ticket: type \"reserve ticket <flight identifier> <seats> <small baggage count> <large baggage count>\"");
        //System.Console.WriteLine("To finish the program, type \"finish\"");
        Console.Write(
            "Searching: type \"search <search term>\"\n" +
            "Sorting: type \"sort <input data> [order]\" (order is optional)\n" +
            "Existing?: type \"exist <airline name>\"\n" +
            "Listing: type \"list <input data> <from>\"\n" +
            "Create route: type \"route new\"\n" +
            "Add flight to route: type \"route add <flight identifier>\"\n" +
            "Remove last route: type \"route remove\"\n" +
            "Print route: type \"route print\"\n" +
            "Route connectivity check: type \"route check <start airport> <end airport>\"\n" +
            "Find shortest route: type \"route search <start airport> <end airport>\"\n" +
            "Find route by strategy: type \"route search <start airport> <end airport> <strategy> (<strategy> options: 'cheap', 'short', 'stops')\"\n" +
            "Reserve cargo: type \"reserve cargo <flight identifier> <cargo weight> <cargo volume>\"\n" +
            "Reserve ticket: type \"reserve ticket <flight identifier> <seats> <small baggage count> <large baggage count>\"\n" +
            "Find a flight route: type \"route find <destination airport>\"\n" +
            "Start batch mode: type \"batch start\"\n" +
            "Execute batch: type \"batch run\"\n" +
            "Cancel batch: type \"batch cancel\"\n" +
            "To finish the program, type \"finish\"\n"
            );
    }
}
