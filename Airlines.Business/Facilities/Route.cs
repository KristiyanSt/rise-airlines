﻿using Airlines.Business.DataStructures;
using Airlines.Business.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airlines.Business.Facilities;
public class Route
{
    private SingleLinkedList<Flight>? _thread;
    public Route()
    {
        this._thread = new SingleLinkedList<Flight>();
    }
    public List<string> ToStringList()
    {
        return this._thread.ToStringList();
    }
    public uint GetCount()
    {
        return this._thread!.Count;
    }
    public void AppendFlight(Flight flight)
    {
        if (this._thread.Count > 0)
        {
            Flight lastFlight = this._thread.GetLast().Value;
            if (lastFlight.ArrivalAirport != flight.DepartureAirport)
            {
                //TODO handle internally caused errors
                throw new InvalidInputException($"{lastFlight.Identifier}'s arrival airport differs from {flight.Identifier}'s departure airport");
            }
        }
        this._thread.AddLast(flight);
    }

    public void PrependFlight(Flight flight)
    {
        if (this._thread.Count > 0)
        {
            Flight firstFlight = this._thread.GetFirst().Value;
            if (flight.ArrivalAirport != firstFlight.DepartureAirport)
            {
                //TODO handle internally caused errors
                throw new InvalidInputException($"{firstFlight.Identifier}'s arrival airport differs from {flight.Identifier}'s departure airport");
            }
        }
        this._thread.AddFirst(flight);
    }
    public void RemoveLastFlight()
    {
        this._thread.RemoveLast();
    }

}
