﻿using Airlines.Business.Exceptions;
using System.Text.RegularExpressions;

namespace Airlines.Business.Facilities;
public class Airport
{
    private string _identifier;
    private string _name;
    private string _city;
    private string _country;
    private static Regex _alphabetAndSpaceRegex = new Regex(@"^[a-zA-Z .]*$");

    public string Identifier
    {
        get => this._identifier;
        set
        {
            value = value.Trim();
            if (string.IsNullOrEmpty(value) || value.Length < 2 || value.Length > 4)
            {
                throw new InvalidInputException($"Invalid airport identifier {value}, it must contain between 2 and 4 characters");
            }
            if (!value.All(char.IsLetterOrDigit))
            {
                throw new InvalidInputException($"Invalid airport identifier {value}, it must contain only alphanumeric symbols");
            }
            this._identifier = value;
        }
    }
    public string Name
    {
        get => this._name;
        set
        {
            value = value.Trim();
            if (string.IsNullOrEmpty(value) || !_alphabetAndSpaceRegex.IsMatch(value))
            {
                throw new InvalidInputException($"Invalid airport name {value}, it can contain only alphabetic and space symbols");
            }
            this._name = value;
        }
    }
    public string City
    {
        get => this._city;
        set
        {
            value = value.Trim();
            if (string.IsNullOrEmpty(value) || !_alphabetAndSpaceRegex.IsMatch(value))
            {
                throw new InvalidInputException($"Invalid airport city {value}, it can contain only alphabetic and space symbols");
            }
            this._city = value;
        }
    }
    public string Country
    {
        get => this._country;
        set
        {
            value = value.Trim();
            if (string.IsNullOrEmpty(value) || !_alphabetAndSpaceRegex.IsMatch(value))
            {
                throw new InvalidInputException($"Invalid airport country {value}, it can contain only alphabetic and space symbols");
            }
            this._country = value;
        }
    }
    //public Airport(string identifier)
    //{
    //    this.Identifier = identifier;
    //}
    public Airport(string identifier, string name, string city, string country)
    {
        this.Identifier = identifier;
        this.Name = name;
        this.City = city;
        this.Country = country;
    }
    public override string ToString() => this.Identifier;
}
