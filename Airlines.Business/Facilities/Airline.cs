﻿using Airlines.Business.Exceptions;

namespace Airlines.Business.Facilities;
public class Airline
{
    private string _name;
    public string Name
    {
        get => this._name;
        set
        {
            value = value.Trim();
            if (value.Length == 0 || value.Length >= 6)
            {
                throw new InvalidInputException($"Invalid airline name {value}, it must have a length between 1 and 5 characters");
            }
            this._name = value;
        }
    }
    public Airline(string name)
    {
        this.Name = name;
    }
    public override string ToString() => this.Name;
}
