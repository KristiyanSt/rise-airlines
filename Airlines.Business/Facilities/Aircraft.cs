﻿using Airlines.Business.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airlines.Business.Facilities;
public class Aircraft
{
    public string Model;
    public Aircraft(string model)
    {
        if (string.IsNullOrEmpty(model))
        {
            throw new InvalidInputException($"Invalid aircraft model {model}");
        }
        this.Model = model;
    }
}
