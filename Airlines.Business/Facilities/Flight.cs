﻿using Airlines.Business.Exceptions;

namespace Airlines.Business.Facilities;
public class Flight
{
    private string _identifier;
    private string _departureAirport;
    private string _arrivalAirport;
    private Aircraft _aircraft;
    private double _price;
    private double _time;

    public Flight(string identifier, string departureAirport, string arrivalAirport, Aircraft aircraft, string price, string time)
    {
        this.Identifier = identifier;
        this.DepartureAirport = departureAirport;
        this.ArrivalAirport = arrivalAirport;
        this.Aircraft = aircraft;

        double priceParsed;
        bool isPriceParsed = double.TryParse(price, out priceParsed);
        if (!isPriceParsed)
        {
            throw new InvalidInputException($"Invalid price of flight {this.Identifier}");
        }
        this._price = priceParsed;

        double timeParsed;
        bool isTimeParsed = double.TryParse(time, out timeParsed);
        if (!isTimeParsed)
        {
            throw new InvalidInputException($"Invalid time of flight {this.Identifier}");
        }
        this._time = timeParsed;
    }
    public string Identifier
    {
        get => this._identifier;
        set
        {
            value = value.Trim();
            if (!value.All(char.IsLetterOrDigit) || value.Length == 0)
            {
                throw new InvalidInputException($"Invalid flight identifier {value}, it must contain only alphanumeric symbols");
            }
            this._identifier = value;
        }
    }
    public string DepartureAirport
    {
        get => this._departureAirport;
        set
        {
            this._departureAirport = value;
        }
    }
    public string ArrivalAirport
    {
        get => this._arrivalAirport;
        set
        {
            this._arrivalAirport = value;
        }
    }
    public Aircraft Aircraft
    {
        get => this._aircraft;
        set => this._aircraft = value;
    }

    public double Price
    {
        get => this._price;
    }
    public double Time
    {
        get => this._time;
    }
    public override string ToString() => this.Identifier;
}
