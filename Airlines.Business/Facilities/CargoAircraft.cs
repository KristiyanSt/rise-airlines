﻿using Airlines.Business.Exceptions;
using System.Xml.Linq;

namespace Airlines.Business.Facilities;
public class CargoAircraft : Aircraft
{
    private double _originalCargoWeightCapacity;
    private double _originalCargoVolumeCapacity;

    public double AvailableCargoWeightCapacity;
    public double AvailableCargoVolumeCapacity;

    public CargoAircraft(string model, string cargoWeightCapacity, string cargoVolumeCapacity) : base(model)
    {
        double weightCapacity;
        bool isWeightCapacityParsed = double.TryParse(cargoWeightCapacity, out weightCapacity);
        if (!isWeightCapacityParsed)
        {
            throw new InvalidInputException($"Invalid cargo capacity of aircraft {model}");
        }
        this._originalCargoWeightCapacity = weightCapacity;
        this.AvailableCargoWeightCapacity = weightCapacity;

        double volumeCapacity;
        bool isVolumeCapacityParsed = double.TryParse(cargoVolumeCapacity, out volumeCapacity);
        if (!isVolumeCapacityParsed)
        {
            throw new InvalidInputException($"Invalid cargo volume of aircraft {model}");
        }
        this._originalCargoVolumeCapacity = volumeCapacity;
        this.AvailableCargoVolumeCapacity = volumeCapacity;
    }

    public bool ReserveCargo(double cargoWeight, double cargoVolume)
    {
        if (this.AvailableCargoWeightCapacity - cargoWeight < 0)
        {
            throw new ArgumentException($"Not enough weight capacity available in {this.Model}");
        }
        if (this.AvailableCargoVolumeCapacity - cargoVolume < 0)
        {
            throw new ArgumentException($"Not enough space available in {this.Model}");
        }
        this.AvailableCargoWeightCapacity -= cargoWeight;
        this.AvailableCargoVolumeCapacity -= cargoVolume;
        return true;
    }
}
