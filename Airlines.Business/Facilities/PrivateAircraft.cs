﻿using Airlines.Business.Exceptions;

namespace Airlines.Business.Facilities;
public class PrivateAircraft : Aircraft
{
    public double CargoCapacity;

    public double CargoVolume;

    public int Seats;
    public PrivateAircraft(string model, string seats) : base(model)
    {
        int seatsCount;
        bool isSeatsParsed = int.TryParse(seats, out seatsCount);
        if (!isSeatsParsed)
        {
            throw new InvalidInputException($"Invalid passenger seats of aircraft {model}");
        }
        this.Seats = seatsCount;
    }
}
