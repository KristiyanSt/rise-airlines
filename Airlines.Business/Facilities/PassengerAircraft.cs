﻿using Airlines.Business.Exceptions;

namespace Airlines.Business.Facilities;
public class PassengerAircraft : Aircraft
{
    //not consistent with data parsing
    private double _originalCargoWeightCapacity;
    private double _originalCargoVolumeCapacity;
    private int _originalSeatsCount;

    private double _smallBaggageMaxWeight = 15;
    private double _smallBaggageMaxVolume = 0.045;
    private double _largeBaggageMaxWeight = 30;
    private double _largeBaggageMaxVolume = 0.090;

    public double AvailableCargoWeightCapacity;
    public double AvailableCargoVolumeCapacity;
    public int AvailableSeatsCount;

    public PassengerAircraft(string model, string cargoCapacity, string cargoVolume, string seats) : base(model)
    {
        double capacity;
        bool isCapacityParsed = double.TryParse(cargoCapacity, out capacity);
        if (!isCapacityParsed)
        {
            throw new InvalidInputException($"Invalid cargo capacity of aircraft {model}");
        }
        this.AvailableCargoWeightCapacity = capacity;
        this._originalCargoWeightCapacity = capacity;

        double volume;
        bool isVolumeParsed = double.TryParse(cargoVolume, out volume);
        if (!isVolumeParsed)
        {
            throw new InvalidInputException($"Invalid cargo volume of aircraft {model}");
        }
        this.AvailableCargoVolumeCapacity = volume;
        this._originalCargoVolumeCapacity = volume;

        int seatsCount;
        bool isSeatsParsed = int.TryParse(seats, out seatsCount);
        if (!isSeatsParsed)
        {
            throw new InvalidInputException($"Invalid passenger seats of aircraft {model}");
        }
        this.AvailableSeatsCount = seatsCount;
        this._originalSeatsCount = seatsCount;
    }

    public bool ReserveTicket(int seats, int smallBaggageCount, int largeBaggageCount)
    {
        if (this.AvailableSeatsCount - seats < 0)
        {
            throw new ArgumentException($"{this.Model}'s seats capacity are not enough to take {seats} seats count");
        }
        double smallBaggageOverallVolume = smallBaggageCount * this._smallBaggageMaxVolume;
        double largeBaggageOverallVolume = largeBaggageCount * this._largeBaggageMaxVolume;
        double overallVolume = smallBaggageOverallVolume + largeBaggageOverallVolume;
        if (this.AvailableCargoVolumeCapacity - overallVolume < 0)
        {
            throw new ArgumentException($"{this.Model}'s volume capacity is not enough to take {smallBaggageCount} small and {largeBaggageCount} large baggages");
        }
        double smallBaggageOverallWeight = smallBaggageCount * this._smallBaggageMaxWeight;
        double largeBaggageOverallWeight = largeBaggageCount * this._largeBaggageMaxWeight;
        double overallWeight = smallBaggageOverallWeight + largeBaggageOverallWeight;
        if (this.AvailableCargoWeightCapacity - overallWeight < 0)
        {
            throw new ArgumentException($"{this.Model}'s weight capacity is not enough to take {smallBaggageCount} small and {largeBaggageCount} large baggages");
        }
        this.AvailableSeatsCount -= seats;
        this.AvailableCargoVolumeCapacity -= overallVolume;
        this.AvailableCargoWeightCapacity -= overallWeight;

        return true;
    }
}
