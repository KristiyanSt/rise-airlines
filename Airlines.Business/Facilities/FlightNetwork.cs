﻿using Airlines.Business.Facilities;
using Airlines.Business.Services;
using Airlines.Business.Strategies;

namespace Airlines.Business.DataStructures;

public class AirportVertex
{
    public Airport Airport { get; set; }
    public List<Flight> Flights { get; set; }
    public AirportVertex(Airport airport)
    {
        this.Airport = airport;
        this.Flights = new List<Flight>();
    }
}
public class FlightNetwork
{
    private List<AirportVertex> AirportVertices;
    private IStrategy _strategy;

    public FlightNetwork(IStrategy strategy)
    {
        this.AirportVertices = new List<AirportVertex>();
        this._strategy = strategy;
    }


    private void PopulateFlights()
    {
        foreach (AirportVertex airportVertex in this.AirportVertices)
        {
            string airportId = airportVertex.Airport.Identifier;
            List<Flight> airportFlights = FlightService.GetFlightsByDepartureAirportId(airportId);
            foreach (Flight flight in airportFlights)
            {
                airportVertex.Flights.Add(flight);
            }
        }
    }
    private void PopulateAirports()
    {
        List<Airport> airports = AirportService.GetAirports();
        airports.ForEach(airport => this.AddAirportVertex(airport));
    }
    private AirportVertex? AddAirportVertex(Airport airport)
    {
        if (this.AirportVertices.Any(x => x.Airport.Identifier == airport.Identifier))
        {
            return null;
        }
        AirportVertex airportVertex = new AirportVertex(airport);
        this.AirportVertices.Add(airportVertex);
        return airportVertex;
    }
    public void PopulateNetwork()
    {
        this.PopulateAirports();
        this.PopulateFlights();
    }

    //private bool DFSTraversal(AirportVertex? currentAirportVertex, string destinationAirport, HashSet<string> visited)
    //{
    //    if (currentAirportVertex == null)
    //    {
    //        return false;
    //    }
    //    if (currentAirportVertex.Airport.Identifier == destinationAirport)
    //    {
    //        return true;
    //    }

    //    visited.Add(currentAirportVertex.Airport.Identifier);

    //    foreach (Flight flight in currentAirportVertex.Flights)
    //    {

    //        if (!visited.Contains(flight.ArrivalAirport))
    //        {
    //            AirportVertex? adjacentAirportVertex =
    //                this.AirportVertices.Find(x => x.Airport.Identifier == flight.ArrivalAirport);

    //            if (adjacentAirportVertex != null)
    //            {
    //                if (DFSTraversal(adjacentAirportVertex, destinationAirport, visited))
    //                {
    //                    return true;
    //                }
    //            }
    //        }
    //    }

    //    return false;
    //}

    //public bool AreAirportsConnected(string startAirportId, string endAirportId)
    //{
    //    AirportVertex? startAirportVertex = this.FindAirportVertexByAirportId(startAirportId);
    //    if (startAirportVertex == null)
    //    {
    //        throw new NotFoundException($"Airport with identifier {startAirportId} does not persist");
    //    }

    //    AirportVertex? endAirportVertex = this.FindAirportVertexByAirportId(endAirportId);
    //    if (endAirportVertex == null)
    //    {
    //        throw new NotFoundException($"Airport with identifier {endAirportVertex} does not persist");
    //    }

    //    return this.DFSTraversal(startAirportVertex, endAirportId, []);
    //}


    public Route? ExecuteStrategy(string startAirportId, string endAirportId)
    {
        return this._strategy.BuildRoute(this.AirportVertices, startAirportId, endAirportId);
    }
    public void SetStrategy(IStrategy strategy)
    {
        this._strategy = strategy;
    }
}
