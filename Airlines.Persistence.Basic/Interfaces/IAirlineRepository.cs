﻿using Airlines.Persistence.Basic.Entities;

namespace Airlines.Persistence.Basic.Interfaces;
public interface IAirlineRepository : IRepository<Airline>
{
}
