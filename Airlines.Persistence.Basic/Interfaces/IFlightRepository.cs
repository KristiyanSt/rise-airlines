﻿using Airlines.Persistence.Basic.Entities;

namespace Airlines.Persistence.Basic.Interfaces;
public interface IFlightRepository : IRepository<Flight>
{
    Task<Flight?> GetFlightWithAirports(int id);
}
