﻿using Airlines.Persistence.Basic.Entities;

namespace Airlines.Persistence.Basic.Interfaces;
public interface IAirportRepository : IRepository<Airport>
{
}