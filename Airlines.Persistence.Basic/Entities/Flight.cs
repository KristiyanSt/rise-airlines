﻿namespace Airlines.Persistence.Basic.Entities;

public partial class Flight
{
    public int Id { get; set; }
    public string FlightNumber { get; set; } = null!;

    public string DepartureAirport { get; set; } = null!;

    public string ArrivalAirport { get; set; } = null!;

    public DateTime DepartureDateTime { get; set; }

    public DateTime ArrivalDateTime { get; set; }

    public virtual Airport ArrivalAirportNavigation { get; set; } = null!;

    public virtual Airport DepartureAirportNavigation { get; set; } = null!;
}
