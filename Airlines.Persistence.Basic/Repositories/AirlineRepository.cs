﻿using Airlines.Persistence.Basic.Context;
using Airlines.Persistence.Basic.Entities;
using Airlines.Persistence.Basic.Interfaces;

namespace Airlines.Persistence.Basic.Repositories;
public class AirlineRepository : Repository<Airline>, IAirlineRepository
{
    public AirlineRepository(AirlinesDbContext airlinesContext) : base(airlinesContext)
    {
    }
}