﻿using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;
using Airlines.Persistence.Basic.Interfaces;

namespace Airlines.Persistence.Basic.Repositories;
public abstract class Repository<TEntity> : IRepository<TEntity> where TEntity : class
{
    protected readonly DbContext _context;
    protected readonly DbSet<TEntity> _table;
    public Repository(DbContext context)
    {
        this._context = context;
        this._table = _context.Set<TEntity>();
    }
    public async Task<TEntity?> GetById(int id)
    {
        return await this._table.FindAsync(id);
    }
    public async Task<TEntity?> SingleOrDefault(Expression<Func<TEntity, bool>> predicate)
    {
        return await this._table.SingleOrDefaultAsync(predicate);
    }

    public IQueryable<TEntity> GetAll()
    {
        return this._table;
    }

    public IQueryable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
    {
        return this._table.Where(predicate);
    }


    public async Task Add(TEntity entity)
    {
        await this._table.AddAsync(entity);
    }

    public async Task AddRange(IEnumerable<TEntity> entities)
    {
        await this._table.AddRangeAsync(entities);
    }

    public void Update(TEntity entity)
    {
        var updated = this._table.Attach(entity);
        this._context.Entry(updated).State = EntityState.Modified;
    }

    public void Remove(TEntity entity)
    {
        this._table.Remove(entity);
    }
    public void RemoveRange(IEnumerable<TEntity> entities)
    {
        this._table.RemoveRange(entities);
    }

    public async Task Complete()
    {
        await this._context.SaveChangesAsync();
    }
}