﻿using Airlines.Persistence.Basic.Context;
using Airlines.Persistence.Basic.Entities;
using Airlines.Persistence.Basic.Interfaces;

namespace Airlines.Persistence.Basic.Repositories;
public class AirportRepository : Repository<Airport>, IAirportRepository
{
    public AirportRepository(AirlinesDbContext airlinesContext) : base(airlinesContext)
    {
    }
}