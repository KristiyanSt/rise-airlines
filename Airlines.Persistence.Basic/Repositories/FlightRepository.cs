﻿using Airlines.Persistence.Basic.Context;
using Airlines.Persistence.Basic.Entities;
using Airlines.Persistence.Basic.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Airlines.Persistence.Basic.Repositories;
public class FlightRepository : Repository<Flight>, IFlightRepository
{
    public FlightRepository(AirlinesDbContext airlinesContext) : base(airlinesContext)
    {
    }

    public async Task<Flight?> GetFlightWithAirports(int id)
    {
        return await _table
            .Include(f => f.ArrivalAirport)
            .Include(f => f.DepartureAirport)
            .FirstOrDefaultAsync(f => f.Id == id);
    }
}
