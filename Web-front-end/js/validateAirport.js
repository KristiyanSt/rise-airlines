﻿const addAirlineForm = document.querySelector('.add-form');
const formSubmitBtn = addAirlineForm.querySelector('.submit');

disableElement(formSubmitBtn);
addValidation(addAirlineForm);
function addValidation(form) {
    Array.from(form.getElementsByTagName('input'))
        .forEach(el => el.addEventListener('blur', (e) => {
            const validation = validations[e.currentTarget.id];
            validation(e.currentTarget);
        }));
}

const validations = {
    "name": validateNameInput,
    "founded": validateFoundedInput,
    "city": validateCityInput,
    "code": validateCodeInput,
    "country": validateCountryInput,
    "runways": validateRunwaysInput
}
const fieldsErrorState = {
    "name": false,
    "founded": false,
    "city": false,
    "code": false,
    "country": false,
    "runways": false
}
function enableElement(el) {
    el.style.opacity = "1";
    el.disabled = false;
}
function disableElement(el) {
    el.style.opacity = "0.6";
    el.disabled = true;
}
function validateNameInput(el) {
    const errors = [];

    if (el.value.length == 0) {
        errors.push("Name is required");
    }
    if (el.value.length > 5) {
        errors.push("Name must consist of less than 5 characters");
    }
    if (el.value.length != 0 && !el.value.match("^[a-zA-Z]+$")) {
        errors.push("Name must consist only alphabetic characters")
    }
    el.parentElement.querySelector('.error').textContent = errors.join("\r\n");

    fieldsErrorState[el.id] = errors.length > 0 ? false : true;
    enableSubmitBtnCheck();
}
function validateFoundedInput(el) {
    let errMessage;

    if (el.value.length == 0) {
        errMessage = "Founded date is required";
    }

    el.parentElement.querySelector('.error').textContent = errMessage;

    fieldsErrorState[el.id] = errMessage ? false : true;
    enableSubmitBtnCheck();
}

function validateCountryInput(el) {
    const errors = [];

    if (el.value.length == 0) {
        errors.push("Country is required");
    }
    if (el.value.length != 0 && !el.value.match("^[a-zA-Z]+$")) {
        errors.push("Country must consist only alphabetic characters")
    }
    el.parentElement.querySelector('.error').textContent = errors.join("\r\n");

    fieldsErrorState[el.id] = errors.length > 0 ? false : true;
    enableSubmitBtnCheck();
}

function validateCityInput(el) {
    const errors = [];

    if (el.value.length == 0) {
        errors.push("City is required");
    }
    if (el.value.length != 0 && !el.value.match("^[a-zA-Z]+$")) {
        errors.push("City must consist only alphabetic characters")
    }
    el.parentElement.querySelector('.error').textContent = errors.join("\r\n");

    fieldsErrorState[el.id] = errors.length > 0 ? false : true;
    enableSubmitBtnCheck();
}

function validateRunwaysInput(el) {
    const errors = [];

    if (el.value.length == 0) {
        errors.push("Runways count size is required");
    }
    if (el.value.length != 0 && !el.value.match("^[0-9]+$")) {
        errors.push("Runways count must consist only numeric characters");
    }

    el.parentElement.querySelector('.error').textContent = errors.join("\r\n");

    fieldsErrorState[el.id] = errors.length > 0 ? false : true;
    enableSubmitBtnCheck();
}

function validateCodeInput(el) {
    const errors = [];

    if (el.value.length == 0) {
        errors.push("Code is required");
    }
    if (el.value.length != 0 && el.value.length != 3) {
        errors.push("Code must contain exactly 3 characters");
    }
    if (el.value.length != 0 && !el.value.match("^[0-9]+$")) {
        errors.push("Code must consist only numeric characters");
    }

    el.parentElement.querySelector('.error').textContent = errors.join("\r\n");

    fieldsErrorState[el.id] = errors.length > 0 ? false : true;
    enableSubmitBtnCheck();
}

function enableSubmitBtnCheck() {
    if (Object.values(fieldsErrorState).every(x => x)) {
        enableElement(formSubmitBtn)
    } else {
        disableElement(formSubmitBtn);
    }
}