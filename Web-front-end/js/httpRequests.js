import requester from './api.js';

const flightsEndpoints = {
    getAll: "flights/all",
    getOne: (flightNumber) => `flights/numbers/${flightNumber}`,
    getById : (id) => `flights/${id}`,
    create: "flights/create",
    edit: "flights/edit",
    delete: (id) => `flights/delete/${id}`
};

getFlights()
    .then(flights => displayFlights(flights));

const tbody = document.querySelector('tbody');
const flightForm = document.querySelector('.flight.add-form');
const formLabel = flightForm.querySelector('.form__title');
const submitBtn = flightForm.querySelector('.submit');

flightForm.addEventListener('submit', handleCreate);

async function populateEditForm(id) {
    const flight = await requester.get(flightsEndpoints.getById(id));

    flightForm.querySelector("#flightNumber").value = flight.flightNumber;
    flightForm.querySelector("#depAirport").value = flight.departureAirport;
    flightForm.querySelector("#arrAirport").value = flight.arrivalAirport;
    flightForm.querySelector("#depTime").value = flight.departureDateTime;
    flightForm.querySelector("#arrTime").value = flight.arrivalDateTime;

    formLabel.textContent = "Edit flight";
    submitBtn.textContent = "Edit";

    flightForm.removeEventListener('submit', handleCreate);
    flightForm.addEventListener('submit', (e) => handleEdit(e, id));
    flightForm.scrollIntoView();
}

function getFlights() {
    return requester.get(flightsEndpoints.getAll)
}

function displayFlights(flights) {
    if (flights) {
        flights.forEach(flight => {
            const flightRow = createFlightRow(flight);
            tbody.appendChild(flightRow);
        });
    } else {
        tbody.replaceChildren(createEmptyRow());
    }
}

function handleCreate(e) {
    e.preventDefault();

    let formData = new FormData(e.target);
    let body = {
        flightNumber: formData.get('flightNumber'),
        departureAirport: formData.get('depAirport'),
        arrivalAirport: formData.get('arrAirport'),
        departureDateTime: formData.get('depTime'),
        arrivalDateTime: formData.get('arrTime'),
    };

    requester.post(flightsEndpoints.create, body)
        .then(() => {
            alert("Successful creation");
            e.target.reset();
        })
        .catch(err => {
            err.then(message => alert(message))
        });

    requester.get(flightsEndpoints.getOne(body.flightNumber))
        .then(flight => {
            console.log(flight);
            const flightRow = createFlightRow(flight);
            tbody.appendChild(flightRow);
        });

    window.scrollTo(0, 0);
}

function handleDelete(e, id) {
    requester.del(flightsEndpoints.delete(id))
        .then(() => {
            alert("Deleted successfully");
        })
        .catch(err => {
            err.then(message => alert(message))
        });

    const currentRow = e.target.parentElement.parentElement;
    currentRow.remove();
}

async function handleEdit(e, id) {
    e.preventDefault();

    let formData = new FormData(e.target);
    let body = {
        id,
        flightNumber: formData.get('flightNumber'),
        departureAirport: formData.get('depAirport'),
        arrivalAirport: formData.get('arrAirport'),
        departureDateTime: formData.get('depTime'),
        arrivalDateTime: formData.get('arrTime'),
    };

    requester.update(flightsEndpoints.edit, body)
        .then(() => {
            alert("Successful edit");
            e.target.reset();
        })
        .catch(err => {
            err.then(message => alert(message))
        });

    e.target.reset();
    formLabel.textContent = "Add new flight";
    submitBtn.textContent = "Submit";

    flightForm.removeEventListener('submit', handleEdit);
    flightForm.addEventListener('submit', (e) => handleCreate(e));
}

function createFlightRow(flight) {
    const flightRow = document.createElement('tr');

    const flightNumberTd = createTableData();
    flightNumberTd.textContent = flight.flightNumber;

    const depAirportTd = createTableData();
    depAirportTd.textContent = flight.departureAirport;

    const arrAirportTd = createTableData();
    arrAirportTd.textContent = flight.arrivalAirport;

    const depDateTd = createTableData();
    depDateTd.textContent = flight.departureDateTime

    const arrDateTd = createTableData();
    arrDateTd.textContent = flight.arrivalDateTime;

    const actionsTd = createTableData();

    const editBtn = document.createElement('button');
    editBtn.textContent = 'Edit';
    editBtn.classList.add('btn');
    editBtn.classList.add('mr-2');
    editBtn.addEventListener('click', () => populateEditForm(flight.id));

    const deleteBtn = document.createElement('button');
    deleteBtn.textContent = 'Delete';
    deleteBtn.classList.add('btn');
    deleteBtn.addEventListener('click', (e) => handleDelete(e, flight.id));

    actionsTd.appendChild(editBtn);
    actionsTd.appendChild(deleteBtn);

    flightRow.append(flightNumberTd, depAirportTd, arrAirportTd, depDateTd, arrDateTd, actionsTd);

    return flightRow;
}

function createEmptyRow() {
    const emptyRow = document.createElement('tr');

    const emptyData = createTableData();
    emptyData.textContent = "No data available";

    emptyRow.appendChild(emptyData);
    for (let i = 0; i < 3; i++) {
        emptyRow.appendChild(createTableData());
    }

    return emptyRow;
}

function createTableData() {
    return document.createElement('td');
}

