const baseUrl = "https://localhost:7292/";

async function request(method, url, data) {
    let options = {
        method,
        headers: {}
    }

    if (data) {
        options.headers['Content-Type'] = 'application/json';
        options.body = JSON.stringify(data);
    }

    return new Promise(async (res, rej) => {
        const response = await fetch(baseUrl + url, options);
        if (response.ok) {
            if (response.status == 204) {
                return res(response);
            }
            return res(response.json());
        }

        rej(response.json());
    });
}
const requester = {
    get: request.bind(null, 'get'),
    post: request.bind(null, 'post'),
    update:request.bind(null, 'put'),
    del: request.bind(null, 'delete')
};
export default requester;