﻿using Airlines.Persistence.Basic.Entities;
using AirlinesWeb.Models;

namespace AirlinesWeb.Extensions;

public static class FlightModelExtensions
{
    public static IQueryable<FlightModel> ToModel(this IQueryable<Flight> flights)
    {
        return flights.Select(f => new FlightModel
        {
            Id = f.Id,
            FlightNumber = f.FlightNumber,
            DepartureAirport = f.DepartureAirport,
            DepartureDateTime = f.DepartureDateTime,
            ArrivalAirport = f.ArrivalAirport,
            ArrivalDateTime = f.ArrivalDateTime
        });
    }
}
