﻿using Airlines.Persistence.Basic.Entities;
using AirlinesWeb.Models;

namespace AirlinesWeb.Extensions;

public static class AirlineModelExtensions
{
    public static IQueryable<AirlineModel> ToModel(this IQueryable<Airline> airlines)
    {
        return airlines.Select(a => new AirlineModel
        {
            Founded = a.Founded,
            Name = a.Name,
            FleetSize = a.FleetSize,
            Description = a.Description,
        });
    }
}
