﻿using Airlines.Persistence.Basic.Entities;
using AirlinesWeb.Models;

namespace AirlinesWeb.Extensions;

public static class AirportModelExtensions
{
    public static IQueryable<AirportModel> ToModel(this IQueryable<Airport> airports)
    {
        return airports.Select(a => new AirportModel
        {
            Country = a.Country,
            City = a.City,
            RunwaysCount = a.RunwaysCount,
            Founded = a.Founded,
            Name = a.Name,
            Code = a.Code
        });
    }
}
