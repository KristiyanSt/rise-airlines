﻿const tableWrapper = document.getElementsByClassName('table-wrapper')[0];
const toggleTableBtn = document.querySelector('.toggle-table');
const showMoreFlightsBtn = document.querySelector('.show-more');
const scrollToTopBtn = document.querySelector('.scroll-top');
const tableArea = document.querySelector('.table-area');

createToggleForTable();
createToggleForForm();
createToggleForLastFlights();
createScrollToTopForTable();
function createToggleForTable() {
    let isTableShown = true;
    toggleTableBtn.addEventListener('click', () => {
        if (isTableShown) {
            tableArea.textContent = "";
            isTableShown = false;
            toggleTableBtn.textContent = "Show table";
            showMoreFlightsBtn.style.visibility = "hidden";
        } else {
            tableArea.appendChild(tableWrapper);
            tableArea.appendChild(scrollToTopBtn);
            toggleTableBtn.textContent = "Hide table";
            showMoreFlightsBtn.style.visibility = "visible";
            isTableShown = true;
        }
    });
}

function createToggleForForm() {
    let isFormShown = true;
    const addForm = document.querySelector('.add-form');
    const toggleFormBtn = document.querySelector('.show-btn');
    toggleFormBtn.addEventListener('click', () => {
        if (isFormShown) {
            addForm.style.visibility = 'hidden';
            toggleFormBtn.textContent = 'Show form';
            isFormShown = false;
        }
        else {
            addForm.style.visibility = 'visible';
            toggleFormBtn.textContent = 'Hide form';
            isFormShown = true;
        }
    });
}

function createToggleForLastFlights() {
    const tbody = document.querySelector('tbody');
    const lastTableRows = [];
    if (tbody.children.length > 3) {
        for (let i = tbody.children.length - 1; i > 2; i--) {
            lastTableRows.push(tbody.children[i]);
            tbody.removeChild(tbody.children[i]);
        }
    }
    let areRowsShown = true;
    showMoreFlightsBtn.addEventListener('click', () => {
        if (areRowsShown) {
            lastTableRows.forEach(row => tbody.appendChild(row));
            showMoreFlightsBtn.textContent = "Hide last flights";
            areRowsShown = false;
        } else {
            for (let i = tbody.children.length - 1; i > 2; i--) {
                tbody.removeChild(tbody.children[i]);
            }
            showMoreFlightsBtn.textContent = "Show more flights";
            areRowsShown = true;
        }
    });
}

function createScrollToTopForTable() {
    document.querySelector('.scroll-top').addEventListener('click', () => {
        tableWrapper.scrollTop = 0;
    });
}