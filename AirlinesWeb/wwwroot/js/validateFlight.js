﻿const addAirlineForm = document.querySelector('.add-form');
const formSubmitBtn = addAirlineForm.querySelector('.submit');

disableElement(formSubmitBtn);
addValidation(addAirlineForm);
function addValidation(form) {
    Array.from(form.getElementsByTagName('input'))
        .forEach(el => el.addEventListener('blur', (e) => {
            const validation = validations[e.currentTarget.id];
            validation(e.currentTarget);
        }));
}

const validations = {
    "flightNumber": validateFlightNumberInput,
    "depAirport": validateDepAirportInput,
    "arrAirport": validateArrAirportInput,
    "depTime": validateDepTimeInput,
    "arrTime": validateArrTimeInput,
}
const fieldsErrorState = {
    "flightNumber": false,
    "depAirport": false,
    "arrAirport": false,
    "depTime": false,
    "arrTime": false,
}
function enableElement(el) {
    el.style.opacity = "1";
    el.disabled = false;
}
function disableElement(el) {
    el.style.opacity = "0.6";
    el.disabled = true;
}
function validateFlightNumberInput(el) {
    const errors = [];

    if (el.value.length == 0) {
        errors.push("Flight number is required");
    }
    if (el.value.length != 0 && !el.value.match("^[a-z0-9]+$")) {
        errors.push("Flight number must consist only alphanumeric characters");
    }

    el.parentElement.querySelector('.error').textContent = errors.join("\r\n");

    fieldsErrorState[el.id] = errors.length > 0 ? false : true;
    enableSubmitBtnCheck();
}
function validateDepTimeInput(el) {
    let errMessage;

    if (el.value.length == 0) {
        errMessage = "Departure date time is required";
    }

    el.parentElement.querySelector('.error').textContent = errMessage;

    fieldsErrorState[el.id] = errMessage ? false : true;
    enableSubmitBtnCheck();
}
function validateArrTimeInput(el) {
    let errMessage;

    if (el.value.length == 0) {
        errMessage = "Arrival date time is required";
    }

    el.parentElement.querySelector('.error').textContent = errMessage;

    fieldsErrorState[el.id] = errMessage ? false : true;
    enableSubmitBtnCheck();
}
function validateDepAirportInput(el) {
    const errors = [];

    if (el.value.length == 0) {
        errors.push("Departure airport is required");
    }

    el.parentElement.querySelector('.error').textContent = errors.join("\r\n");

    fieldsErrorState[el.id] = errors.length > 0 ? false : true;
    enableSubmitBtnCheck();
}
function validateArrAirportInput(el) {
    const errors = [];

    if (el.value.length == 0) {
        errors.push("Arrival airport is required");
    }

    el.parentElement.querySelector('.error').textContent = errors.join("\r\n");

    fieldsErrorState[el.id] = errors.length > 0 ? false : true;
    enableSubmitBtnCheck();
}

function enableSubmitBtnCheck() {
    if (Object.values(fieldsErrorState).every(x => x)) {
        enableElement(formSubmitBtn)
    } else {
        disableElement(formSubmitBtn);
    }
}