﻿using Airlines.Business.DTOs;
using Airlines.Business.Interfaces;
using AirlinesWeb.Extensions;
using AirlinesWeb.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;


namespace AirlinesWeb.Controllers;
public class AirportsController : Controller
{
    private readonly IAirportService _airportService;
    public AirportsController(IAirportService airportService)
    {
        this._airportService = airportService;
    }

    public async Task<IActionResult> Index(AirportDTO airportDTO, string? search)
    {
        return View(await _CreateAirportsListAndFormModel(airportDTO));
    }

    [HttpPost]
    public async Task<IActionResult> Index([FromForm] AirportDTO airportDTO)
    {
        if (!ModelState.IsValid)
        {
            return View(await _CreateAirportsListAndFormModel(airportDTO));
        }

        try
        {
            await this._airportService.AddAirport(airportDTO);
        }
        catch (Exception e)
        {
            ViewData["errMessage"] = e.Message;
        }

        return View(await _CreateAirportsListAndFormModel(new AirportDTO()));
    }

    private async Task<AirportsListAndFormModel> _CreateAirportsListAndFormModel(AirportDTO airportDTO)
    {
        var airports = await this._airportService
           .GetAll()
           .ToModel()
           .ToListAsync();

        return new AirportsListAndFormModel(airports, airportDTO);
    }
}
