﻿using Airlines.Business.DTOs;
using Airlines.Business.Interfaces;
using AirlinesWeb.Extensions;
using AirlinesWeb.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace AirlinesWeb.Controllers;
public class FlightsController : Controller
{
    private readonly IFlightService _flightService;
    public FlightsController(IFlightService flightService)
    {
        this._flightService = flightService;
    }
    public async Task<IActionResult> Index(FlightDTO flightDTO, string? search)
    {
        return View(await _CreateFlightListAndFormModel(flightDTO));
    }

    [HttpPost]
    public async Task<IActionResult> Index([FromForm] FlightDTO flightDTO)
    {
        if (!ModelState.IsValid)
        {
            return View(await _CreateFlightListAndFormModel(flightDTO));
        }

        try
        {
            await this._flightService.AddFlight(flightDTO);
        }
        catch (Exception e)
        {
            ViewData["errMessage"] = e.Message;
        }

        return View(await _CreateFlightListAndFormModel(new FlightDTO()));
    }

    private async Task<FlightListAndFormModel> _CreateFlightListAndFormModel(FlightDTO flightDTO)
    {
        var flights = await this._flightService
            .GetAll()
            .ToModel()
            .ToListAsync();

        return new FlightListAndFormModel(flights, flightDTO);
    }
}
