using Airlines.Business.Interfaces;
using AirlinesWeb.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace AirlinesWeb.Controllers;
public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;
    private readonly IAirlineService _airlineService;
    private readonly IFlightService _flightService;
    private readonly IAirportService _airportService;
    public HomeController(
        ILogger<HomeController> logger,
        IAirlineService airlineService,
        IAirportService airportService,
        IFlightService flightService
        )
    {
        this._logger = logger;
        this._airlineService = airlineService;
        this._airportService = airportService;
        this._flightService = flightService;
    }

    public IActionResult Index()
    {
        int airlinesCount = this._airlineService.GetAll().Count();
        int airportsCount = this._airportService.GetAll().Count();
        int flightsCount = this._flightService.GetAll().Count();

        ViewData["airlinesCount"] = airlinesCount;
        ViewData["airportsCount"] = airportsCount;
        ViewData["flightsCount"] = flightsCount;

        return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}
