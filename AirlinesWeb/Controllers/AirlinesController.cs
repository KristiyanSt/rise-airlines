﻿using Airlines.Business.DTOs;
using Airlines.Business.Interfaces;
using AirlinesWeb.Extensions;
using AirlinesWeb.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace AirlinesWeb.Controllers;
public class AirlinesController : Controller
{
    private readonly IAirlineService _airlineService;
    private readonly int _PAGE_SIZE = 7;
    public AirlinesController(IAirlineService airlineService)
    {
        _airlineService = airlineService;
    }
    public async Task<IActionResult> Index(AirlineDTO airlineDTO, string? search, string? isAfter2008, int page = 1)
    {

        var airlinesListAndFormModel = await _CreateAirlinesListAndFormModelByQueries(airlineDTO, search, isAfter2008, page);
        return View(airlinesListAndFormModel);
    }

    [HttpPost]
    public async Task<IActionResult> Index([FromForm] AirlineDTO airlineDTO)
    {
        if (!ModelState.IsValid)
        {
            return View(await _CreateAirlinesListAndFormModelByQueries(airlineDTO, null, null));
        }

        try
        {
            await _airlineService.AddAirline(airlineDTO);
        }
        catch (Exception e)
        {
            ViewData["errMessage"] = e.Message;
        }

        var airlinesListAndFormModel = await _CreateAirlinesListAndFormModelByQueries(new AirlineDTO(), null, null);
        return View(airlinesListAndFormModel);
    }

    private async Task<AirlinesListAndFormModel> _CreateAirlinesListAndFormModelByQueries(AirlineDTO airlineDTO, string? search, string? isAfter2008, int page = 1)
    {
        var airlinesQuery = this._airlineService
            .GetAll();

        if (search != null)
        {
            airlinesQuery = airlinesQuery
                 .Where(a => a.Name.Contains(search));
        }

        if (isAfter2008 != null)
        {
            airlinesQuery = airlinesQuery
                .Where(a => a.Founded.Year > 2008);
        }

        int totalCount = await airlinesQuery.CountAsync();
        int totalPages = (int)Math.Ceiling(((decimal)totalCount / _PAGE_SIZE));
        var paginationModel = new PaginationModel(totalCount, totalPages, page);

        airlinesQuery = airlinesQuery.Skip((page - 1) * _PAGE_SIZE).Take(_PAGE_SIZE);

        var airlines = await airlinesQuery.ToModel().ToListAsync();
        return new AirlinesListAndFormModel(airlines, paginationModel, airlineDTO);
    }


}
