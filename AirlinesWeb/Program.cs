using Airlines.Persistence.Basic.Context;
using Airlines.Persistence.Basic.Interfaces;
using Airlines.Persistence.Basic.Repositories;
using Airlines.Business.Interfaces;

using Microsoft.EntityFrameworkCore;
using Airlines.Business.WebServices;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();

builder.Services.AddDbContext<AirlinesDbContext>((options => options.UseSqlServer(builder.Configuration.GetConnectionString("Local"))))
                .AddScoped<IFlightRepository, FlightRepository>()
                .AddScoped<IAirlineRepository, AirlineRepository>()
                .AddScoped<IAirportRepository, AirportRepository>()
                .AddScoped<IFlightService, FlightService>()
                .AddScoped<IAirportService, AirportService>()
                .AddScoped<IAirlineService, AirlineService>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();
