﻿using Airlines.Business.DTOs;

namespace AirlinesWeb.Models;

public class AirlinesListAndFormModel
{
    public AirlinesListAndFormModel(
        List<AirlineModel> airlines,
        PaginationModel paginationModel,
        AirlineDTO airlineDTO)
    {
        this.Airlines = airlines;
        this.PaginationModel = paginationModel;
        this.AirlineDTO = airlineDTO;
    }
    public List<AirlineModel> Airlines { get; set; }
    public AirlineDTO AirlineDTO { get; set; }
    public PaginationModel PaginationModel { get; set; }
}
