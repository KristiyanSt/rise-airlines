﻿namespace AirlinesWeb.Models;

public class AirportModel
{
    public string Code { get; set; }

    public string Name { get; set; }

    public string City { get; set; }

    public string Country { get; set; }

    public int RunwaysCount { get; set; }

    public DateTime Founded { get; set; }
}

