﻿using Airlines.Business.DTOs;

namespace AirlinesWeb.Models;

public class AirportsListAndFormModel
{
    public AirportsListAndFormModel(List<AirportModel> airports, AirportDTO airportDTO)
    {
        this.Airports = airports;
        this.AirportDTO = airportDTO;
    }
    public List<AirportModel> Airports { get; set; }
    public AirportDTO AirportDTO { get; set; }
}
