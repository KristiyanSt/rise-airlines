﻿namespace AirlinesWeb.Models;

public class AirlineModel
{
    public string Name { get; set; }

    public DateTime Founded { get; set; }

    public int FleetSize { get; set; }
    public string Description { get; set; }
}
