﻿namespace AirlinesWeb.Models;

public class PaginationModel
{
    public PaginationModel(int totalCount, int totalPages, int page)
    {
        this.TotalCount = totalCount;
        this.TotalPages = totalPages;
        this.Page = page;
    }
    public int TotalCount { get; set; }
    public int TotalPages { get; set; }
    public int Page { get; set; }

    public bool IsFirstPage()
    {
        return this.Page == 1;
    }

    public bool IsLastPage()
    {
        return this.Page == this.TotalPages;
    }

    public bool HasPages()
    {
        return this.TotalPages > 1;
    }
}
