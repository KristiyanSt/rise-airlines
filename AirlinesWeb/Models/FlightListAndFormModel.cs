﻿using Airlines.Business.DTOs;

namespace AirlinesWeb.Models;

public class FlightListAndFormModel
{
    public FlightListAndFormModel(List<FlightModel> flights, FlightDTO flightDTO)
    {
        this.Flights = flights;
        this.FlightDTO = flightDTO;
    }
    public List<FlightModel> Flights { get; set; }
    public FlightDTO FlightDTO { get; set; }
}
