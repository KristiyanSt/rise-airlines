CREATE TABLE Flights (
    FlightNumber NVARCHAR(250) NOT NULL,
    DepartureAirport CHAR(3) NOT NULL,
    ArrivalAirport CHAR(3) NOT NULL,
    DepartureDateTime DATETIME NOT NULL,
    ArrivalDateTime DATETIME NOT NULL
);

ALTER TABLE Flights
ADD ID INT IDENTITY(1,1) PRIMARY KEY;

ALTER TABLE Flights
ADD CONSTRAINT CHECK_DepartureInFuture CHECK (GETDATE() < DepartureDateTime);

ALTER TABLE Flights
ADD CONSTRAINT CHECK_ArrivalInFuture CHECK (GETDATE() < ArrivalDateTime);

ALTER TABLE Flights
ADD CONSTRAINT CHECK_DepartureBeforeArrival CHECK (DepartureDateTime < ArrivalDateTime);


ALTER TABLE Flights
ADD CONSTRAINT FK_DepartureAirport
FOREIGN KEY (DepartureAirport)
REFERENCES Airports(Code);

ALTER TABLE Flights
ADD CONSTRAINT FK_ArrivalAirport
FOREIGN KEY (ArrivalAirport)
REFERENCES Airports(Code);