USE AirlinesDB;

INSERT INTO Flights (FlightNumber, DepartureAirport,ArrivalAirport,DepartureDateTime, ArrivalDateTime)
	VALUES ('FL123', 'DFW', 'ATL', '2024-04-14 14:20:00', '2024-04-14 15:20:00');

INSERT INTO Flights (FlightNumber, DepartureAirport,ArrivalAirport,DepartureDateTime, ArrivalDateTime)
	VALUES ('FL456', 'ATL', 'ORD', '2024-04-14 18:00:00', '2024-04-14 21:20:00');

SELECT * FROM Flights 
WHERE FlightNumber = 'FL123';

UPDATE Flights SET ArrivalAirport = 'ORD' WHERE FlightNumber = 'FL123';

DELETE FROM Flights WHERE FlightNumber = 'FL123';


