INSERT INTO Airports(Code, Name, City, Country, RunwaysCount, Founded)
	VALUES ('DFW', 'Donald F. Wild', 'Chicago', 'USA', '3', SYSDATETIME());

INSERT INTO Airports(Code, Name, City, Country, RunwaysCount, Founded)
	VALUES ('ATL', 'Atlanta Terrible Line', 'Ilinois', 'USA', '5', SYSDATETIME());

INSERT INTO Flights (FlightNumber, DepartureAirport, ArrivalAirport, DepartureDateTime, ArrivalDateTime)
	VALUES ('FL123', 'DFW', 'ATL', '2024-04-16 18:20:00', '2024-04-16 20:20:00');

--failing with invalid departure airport code
INSERT INTO Flights (FlightNumber, DepartureAirport, ArrivalAirport, DepartureDateTime, ArrivalDateTime)
	VALUES ('FL456', 'ZDR', 'ORD', '2024-04-16 18:20:00', '2024-04-16 20:20:00');

--failing with invalid arrival airport code
INSERT INTO Flights (FlightNumber, DepartureAirport, ArrivalAirport, DepartureDateTime, ArrivalDateTime)
	VALUES ('FL456', 'DFW', 'NSH', '2024-04-16 18:20:00', '2024-04-16 20:20:00');

--failing with departure time after arrival time
INSERT INTO Flights (FlightNumber, DepartureAirport, ArrivalAirport, DepartureDateTime, ArrivalDateTime)
	VALUES ('FL456', 'DFW', 'NSH', '2024-04-16 18:00:00', '2024-04-16 17:50:00');

--failing with past departure airport time
INSERT INTO Flights (FlightNumber, DepartureAirport, ArrivalAirport, DepartureDateTime, ArrivalDateTime)
	VALUES ('FL456', 'DFW', 'NSH', '2024-04-14 18:00:00', '2024-04-16 19:00:00');

--failing with past arrival airport time
INSERT INTO Flights (FlightNumber, DepartureAirport, ArrivalAirport, DepartureDateTime, ArrivalDateTime)
	VALUES ('FL456', 'DFW', 'NSH', '2024-04-16 18:00:00', '2024-04-14 19:00:00');

