--total count of all departed flights
SELECT COUNT(*) AS totalCountOfDepartedFlights FROM Flights
WHERE DepartureDateTime < GETDATE() AND ArrivalDateTime > GETDATE();

--all flights joined by airports, where departure time is scheduled for tomorrow
SELECT * FROM Flights AS f
JOIN Airports AS dep ON f.DepartureAirport = dep.Code
JOIN Airports AS arr ON f.ArrivalAirport = arr.Code
WHERE CONVERT(DATE, f.DepartureDateTime) = CONVERT(DATE, DATEADD(DAY, 1, GETDATE()));

--the longest flight in seconds representing the most farther flight
SELECT TOP 1 f.FlightNumber FROM Flights AS f
ORDER BY DATEDIFF(SECOND, f.DepartureDateTime, f.ArrivalDateTime) DESC


