
# Airlines Project

Airlines Project is a C# console application with main purpose of processing user input of airports, airlines and flights and outputting valid data based on defined criteria.

# Key Features
- Reading and storing user input for airports, airlines, and flights.
- Validating input based on defined criteria.
- Printing validated data in an organized format.